use std::io;

#[derive(Clone, Debug)]
pub enum CliError {
    IO(String),
}

impl From<io::Error> for CliError {
    fn from(error: io::Error) -> Self {
        CliError::IO(error.to_string())
    }
}