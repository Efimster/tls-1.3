mod cli_error;

use std::net::{TcpListener};
use std::io;
use tls::structs::tls_stream::TLSStream;
use std::io::{Write, BufRead, BufReader};
use std::str;
use x509::utils::file_util::{read_private_key, read_certificate_file};

const LISTENING_SOCKET_ADDR: &str = "127.0.0.1:7978";
const CRLF: &[u8] = &[0xd, 0xa];

fn main() {
    let listener = TcpListener::bind(LISTENING_SOCKET_ADDR).unwrap();
    println!("listen {}", LISTENING_SOCKET_ADDR);

    let mut certificates_chain:Vec<&[u8]> = Vec::with_capacity(2);
    let server_certificate:&[u8] = &read_certificate_file("res/trytls2.cer").expect("< can't read certificate");
    let ca_certificate:&[u8] = &read_certificate_file("res/trytlsca.cer").expect("< can't read ca certificate");
    certificates_chain.push(&server_certificate);
    certificates_chain.push(&ca_certificate);
    let private_key = read_private_key("res/ecdsa-private-key.pem", "ecdsa");
//    let private_key = read_private_key("res/rsa-privatekey.pem", "rsa");

    for stream in TLSStream::incoming(&listener, &certificates_chain, &private_key) {
        match stream {
            Ok(mut stream) => {
                println!("Incoming connection {}", stream.peer_addr().unwrap().ip());
                //stream.set_read_timeout(Some(Duration::from_millis(50))).unwrap();
                let mut reader = BufReader::new(stream.reader);
                match run(&mut reader, &mut stream.writer) {
                    Ok(_) => (),
                    Err(err) => panic!(err),
                }
            },
            Err(error) => println!("connection establishment error: {}", error),
        }
        println!("Incoming connection terminated");
    }
}

fn run<R: BufRead, W: Write>(reader: &mut R, writer: &mut W) -> io::Result<()> {
    let request = read_request(reader)?;
    println!("{}", str::from_utf8(&request).unwrap());

    send_response(writer)?;

    Ok(())
}

fn read_request<R: BufRead>(reader: &mut R) -> io::Result<Vec<u8>> {
    let mut content_length = 0usize;
    let mut chunked_encoding = false;
    let mut line: String = String::new();
    loop {
        line.clear();
        reader.read_line(&mut line)?;
        print!("{}", line);
        if line.len() == 2 && line.as_bytes() == CRLF {
            break;
        }
        let line: Vec<&str> = line.splitn(2, ':').collect();
        if line[0].trim().to_lowercase().as_str() == "content-length" {
            content_length = line[1].trim().parse().unwrap();
        }
        if line[0].trim().to_lowercase().as_str() == "transfer-encoding" && line[1].trim().to_lowercase().as_str() != "identity" {
            chunked_encoding = true;
        }
    }
    let response = if chunked_encoding {
        read_chunked_message(reader)?
    } else {

        let mut response = Vec::new();
        let mut buf = [0u8; 500];
        while response.len() < content_length {
            let length = reader.read(&mut buf)?;
            response.extend_from_slice(&buf[..length]);
        }
        response
    };

    Ok(response)
}


fn send_response<W: Write>(writer: &mut W) -> io::Result<()>{
    let message = "<!doctype html>
<html>
<body>
    Hello from TLS v1.3 connection!!!
</body>
</html>";
    let mut response = Vec::new();
    add_http_line(&mut response, "HTTP/1.1 200 OK");
    add_http_line(&mut response, "Server: test/0.1");
    add_http_line(&mut response, "Content-Type: text/html; charset=utf-8");
    add_http_line(&mut response, &format!("content-length: {:x}", message.len()));
    add_http_line(&mut response, "");
    response.extend_from_slice(message.as_bytes());
    writer.write(&response)?;
    Ok(())
}

fn add_http_line(to:& mut Vec<u8>, line:&str) {
    to.extend_from_slice(line.as_bytes());
    to.extend_from_slice(CRLF);
}

fn read_chunked_message<R: BufRead>(reader: &mut R) -> io::Result<Vec<u8>> {
    let mut response = Vec::new();
    let mut line: String = String::new();
    loop {
        reader.read_line(&mut line)?;
        let size_str = line.split(" ").collect::<Vec<&str>>()[0].trim();
        let chunk_length = usize::from_str_radix(size_str, 16).unwrap();
        line.clear();
        if chunk_length == 0 {
            break;
        }
        let length = reader.read_until(0xd, &mut response)?;
        debug_assert_eq!(chunk_length, length - 1);
        reader.read_line(&mut line)?;
    }

    loop {
        reader.read_line(&mut line)?;
        if line.len() == 2 && line.as_bytes() == &[0xd, 0xa] {
            break;
        }
    }

    Ok(response)
}