use std::io::{Write, BufReader, BufRead};
use tls::structs::tls_stream::TLSStream;
use std::io;
use std::usize;


//facebook.com
//const CONNECT_SOCKET_ADDR: &str = "157.240.30.35:443";
//const CONNECT_SOCKET_ADDR: &str = "127.0.0.1:7978";d151l6v8er5bdm.cloudfront.net

const CONNECT_SOCKET_ADDR: &str = "35.225.163.227:443";
const CRLF: &[u8] =  &[0xd, 0xa];

fn main() {
    let mut stream = TLSStream::connect(CONNECT_SOCKET_ADDR).expect("Failed to connect");
    let mut reader = BufReader::new(stream.reader);
    println!("Successfully connected to server {}", CONNECT_SOCKET_ADDR);
    match run(&mut reader, &mut stream.writer) {
        Ok(_) => (),
        Err(err) => panic!(err),
    }
    println!("Terminated.");
}

fn run<R:BufRead,  W:Write>(reader:&mut R, writer:&mut W) -> io::Result<()> {
    send_request(writer)?;
    let response = read_response(reader)?;

    println!("response {} bytes", response.len());
    Ok(())
}

fn send_request<W: Write>(writer: &mut W) -> io::Result<()>{
    let mut request = Vec::new();
    add_http_line(&mut request, "GET / HTTP/1.1");
    add_http_line(&mut request, "Host: www.facebook.com");
    add_http_line(&mut request, "user-agent: test-agent");
    add_http_line(&mut request, "accept: text/html;q=0.9,*/*;q=0.8");
    add_http_line(&mut request, "accept-language: en-us");
    add_http_line(&mut request, "content-length: 0");
    add_http_line(&mut request, "");

    writer.write(&request)?;
    Ok(())
}

fn read_response<R: BufRead>(reader: &mut R) -> io::Result<Vec<u8>>{
    let mut content_length = 0usize;
    let mut chunked_encoding = false;
    let mut line: String = String::new();
    loop {
        line.clear();
        reader.read_line(&mut line)?;
        print!("{}", line);
        if line.len() == 2 && line.as_bytes() == CRLF{
            break;
        }
        let line: Vec<&str> = line.splitn(2, ':').collect();
        if line[0].trim().to_lowercase().as_str() == "content-length" {
            content_length = line[1].trim().parse().unwrap();
        }
        if line[0].trim().to_lowercase().as_str() == "transfer-encoding" && line[1].trim().to_lowercase().as_str() != "identity"{
            chunked_encoding = true;
        }
    }
    let response = if chunked_encoding {
        read_chunked_message(reader)?
    }
    else {
        let mut response = Vec::new();
        let mut buf= [0u8; 500];
        while response.len() < content_length {
            let length = reader.read(&mut buf)?;
            response.extend_from_slice(&buf[..length]);
        }
        response
    };

    Ok(response)
}

fn read_chunked_message<R: BufRead>(reader: &mut R) -> io::Result<Vec<u8>>{
    let mut response = Vec::new();
    let mut line: String = String::new();
    loop  {
        reader.read_line(&mut line)?;
        let size_str = line.split(" ").collect::<Vec<&str>>()[0].trim();
        let chunk_length = usize::from_str_radix(size_str, 16).unwrap();
        line.clear();
        if chunk_length == 0 {
            break;
        }
        let length = reader.read_until(0xd, &mut response)?;
        debug_assert_eq!(chunk_length, length - 1);
        reader.read_line(&mut line)?;
    }

    loop {
        reader.read_line(&mut line)?;
        if line.len() == 2 && line.as_bytes() == &[0xd, 0xa] {
            break;
        }
    }

    Ok(response)
}

fn add_http_line(to: &mut Vec<u8>, line: &str) {
    to.extend_from_slice(line.as_bytes());
    to.extend_from_slice(CRLF);
}