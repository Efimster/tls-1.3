use crate::utils::transmute;
use crate::utils::transmute::EncodeTo;
use crate::utils::transmute::DecodeFrom;
use crate::enums::tls_error::TLSError;

const TLS10: u16 = 0x0301;
const TLS11: u16 = 0x0302;
const TLS12: u16 = 0x0303;
const TLS13: u16 = 0x0304;

#[derive(Debug, PartialOrd, PartialEq)]
pub enum ProtocolVersion {
    TLS10,
    TLS11,
    TLS12,
    TLS13,
    Unknown(u16),
}

impl<'a> DecodeFrom<'a> for ProtocolVersion {
    fn decode(bytes:&[u8]) -> Result<(ProtocolVersion, usize, &[u8]), TLSError>{
        let version = match transmute::u16_from_bytes_be(bytes) {
            TLS13 => ProtocolVersion::TLS13,
            TLS12 => ProtocolVersion::TLS12,
            TLS11 => ProtocolVersion::TLS11,
            TLS10 => ProtocolVersion::TLS10,
            value @ _ => ProtocolVersion::Unknown(value),
        };
        Ok((version, 2, &bytes[2 .. ]))
    }
}

impl EncodeTo for ProtocolVersion {
    fn encode_to(&self, to: &mut Vec<u8>) {
        let value = match self {
            ProtocolVersion::TLS13 => TLS13,
            ProtocolVersion::TLS12 => TLS12,
            ProtocolVersion::TLS11 => TLS11,
            ProtocolVersion::TLS10 => TLS10,
            ProtocolVersion::Unknown(value) => *value,
        };
        to.extend_from_slice(&transmute::u16_to_bytes_be(value));
    }
}

