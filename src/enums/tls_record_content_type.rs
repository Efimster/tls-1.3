use crate::enums::tls_error::TLSError;
use crate::enums::alert_description::AlertDescription;
use crate::utils::transmute::DecodeFrom;
use crate::utils::transmute::EncodeTo;

pub const CHANGE_CIPHER_SPEC: u8 = 20;
pub const ALERT: u8 = 21;
pub const HANDSHAKE: u8 = 22;
pub const APPLICATION_DATA: u8 = 23;

#[derive(Debug, PartialOrd, PartialEq, Copy, Clone)]
pub enum TLSRecordContentType {
    Alert,
    Handshake,
    ApplicationData,
    ChangeCipherSpec,
}

impl<'a> DecodeFrom<'a> for TLSRecordContentType {

    fn decode(bytes:&[u8]) -> Result<(TLSRecordContentType, usize, &[u8]), TLSError>{
        let result = match bytes[0] {
            ALERT => TLSRecordContentType::Alert,
            HANDSHAKE => TLSRecordContentType::Handshake,
            APPLICATION_DATA => TLSRecordContentType::ApplicationData,
            CHANGE_CIPHER_SPEC => TLSRecordContentType::ChangeCipherSpec,
            _ => return Err(TLSError::Alert(AlertDescription::DecodeError))
        };
        Ok((result, 1usize, &bytes[1..]))
    }
}

impl EncodeTo for TLSRecordContentType {
    fn encode_to(&self, to: &mut Vec<u8>) {
        let value = match self {
            TLSRecordContentType::ApplicationData => APPLICATION_DATA,
            TLSRecordContentType::Handshake => HANDSHAKE,
            TLSRecordContentType::Alert => ALERT,
            TLSRecordContentType::ChangeCipherSpec => CHANGE_CIPHER_SPEC,
        };
        to.push(value);
    }
}