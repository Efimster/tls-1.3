use crate::utils::transmute;
use crate::utils::transmute::EncodeTo;
use crate::utils::transmute::DecodeFrom;
use crate::enums::tls_error::TLSError;


const TLS_AES_128_GCM_SHA256: u16 = 0x1301;
const TLS_AES_256_GCM_SHA384: u16 = 0x1302;
const TLS_CHACHA20_POLY1305_SHA256: u16 = 0x1303;
const TLS_AES_128_CCM_SHA256: u16 = 0x1304;
const TLS_AES_128_CCM_8_SHA256: u16 = 0x1305;

#[derive(Debug, Clone, Copy)]
pub enum CipherSuite {
    Aes128GcmSha256,
    Aes256GcmSha384,
    ChaCha20Poly1305Sha256,
    Aes128CcmSha256,
    Aes128Ccm8Sha256,
    Unknown(u16),
}

impl<'a> DecodeFrom<'a> for CipherSuite {
    fn decode(bytes:&[u8]) -> Result<(CipherSuite, usize, &[u8]), TLSError> {
        let value = match transmute::u16_from_bytes_be(bytes) {
            TLS_AES_128_GCM_SHA256 => CipherSuite::Aes128GcmSha256,
            TLS_AES_256_GCM_SHA384 => CipherSuite::Aes256GcmSha384,
            TLS_CHACHA20_POLY1305_SHA256 => CipherSuite::ChaCha20Poly1305Sha256,
            TLS_AES_128_CCM_SHA256 => CipherSuite::Aes128CcmSha256,
            TLS_AES_128_CCM_8_SHA256 => CipherSuite::Aes128Ccm8Sha256,
            value => CipherSuite::Unknown(value),
        };
        Ok((value, 2, &bytes[2..]))
    }
}

impl EncodeTo for CipherSuite {
    fn encode_to(&self, to: &mut Vec<u8>) {
        let value = match self {
            CipherSuite::Aes128GcmSha256 => TLS_AES_128_GCM_SHA256,
            CipherSuite::Aes256GcmSha384 => TLS_AES_256_GCM_SHA384,
            CipherSuite::ChaCha20Poly1305Sha256 => TLS_CHACHA20_POLY1305_SHA256,
            CipherSuite::Aes128CcmSha256 => TLS_AES_128_CCM_SHA256,
            CipherSuite::Aes128Ccm8Sha256 => TLS_AES_128_CCM_8_SHA256,
            CipherSuite::Unknown(value) => *value,
        };
        to.extend_from_slice(&transmute::u16_to_bytes_be(value));
    }
}