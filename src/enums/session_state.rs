pub enum ServerSessionState {
    Start,
    ReceivedClientHello,
    Negotiated,
    WaitEndOfEarlyData,
    WaitCertificate,
    WaitCertificateVerify,
    WaitFinished,
    Connected
}

pub enum ClientSessionState {
    Start,
    WaitServerHello,
    WaitEncryptedExtensions,
    WaitCertificate,
    WaitCertificateVerify,
    WaitFinished,
    Connected
}
