use crate::utils::transmute;
use crate::utils::transmute::EncodeTo;
use crate::utils::transmute::DecodeFrom;
use crate::enums::tls_error::TLSError;
use std::str;

const HTTP11:&str = "http/1.1";
const HTTP2: &str = "h2";
const WEB_RTC: &str = "webrtc";
const FTP: &str = "ftp";
const IMAP: &str = "imap";
const POP3: &str = "pop3";

//https://www.iana.org/assignments/tls-extensiontype-values/tls-extensiontype-values.xhtml#alpn-protocol-ids
#[derive(Debug)]
pub enum ProtocolName {
    Http11,
    Http2,
    WebRTC,
    Ftp,
    Imap,
    Pop3,
    Other(String)
}

impl<'a> DecodeFrom<'a> for ProtocolName {
    fn decode(bytes:&[u8]) -> Result<(ProtocolName, usize, &[u8]), TLSError>{
        let (entry, size, bytes) = transmute::decode_slice(bytes, 1);
        let name = str::from_utf8(entry)?;
        let name = match name {
            HTTP11 => ProtocolName::Http11,
            HTTP2 => ProtocolName::Http2,
            WEB_RTC => ProtocolName::WebRTC,
            FTP => ProtocolName::Ftp,
            IMAP => ProtocolName::Imap,
            POP3 => ProtocolName::Pop3,
            _ => ProtocolName::Other(name.to_string()),
        };
        Ok((name, size, bytes))
    }
}

impl EncodeTo for ProtocolName {
    fn encode_to(&self, to: &mut Vec<u8>) {
        match self {
            ProtocolName::Http11 =>
                transmute::encode_slice_to(to, HTTP11.as_bytes(), 1),
            ProtocolName::Http2 =>
                transmute::encode_slice_to(to, HTTP2.as_bytes(), 1),
            ProtocolName::WebRTC =>
                transmute::encode_slice_to(to, WEB_RTC.as_bytes(), 1),
            ProtocolName::Ftp =>
                transmute::encode_slice_to(to, FTP.as_bytes(), 1),
            ProtocolName::Imap =>
                transmute::encode_slice_to(to, IMAP.as_bytes(), 1),
            ProtocolName::Pop3 =>
                transmute::encode_slice_to(to, POP3.as_bytes(), 1),
            ProtocolName::Other(ref value) =>
                transmute::encode_slice_to(to, value.as_bytes(), 1),
        }
    }
}