use crate::enums::tls_error::TLSError;
use crate::enums::alert_description::AlertDescription;
use crate::utils::transmute::DecodeFrom;
use crate::utils::transmute::EncodeTo;

#[derive(Debug)]
pub enum NameType {
    HostName
}

impl EncodeTo for NameType {
    fn encode_to(&self, to: &mut Vec<u8>) {
        match self {
            NameType::HostName => to.push(0),
        }
    }
}

impl<'a> DecodeFrom<'a> for NameType {
    fn decode(bytes:&[u8]) -> Result<(NameType, usize, &[u8]), TLSError> {
        match bytes[0] {
            0 => Ok((NameType::HostName, 1, &bytes[1..])),
            _ => Err(TLSError::Alert(AlertDescription::DecodeError)),
        }
    }
}