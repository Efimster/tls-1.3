use crate::utils::transmute;
use crate::utils::transmute::DecodeFrom;
use crate::utils::transmute::EncodeTo;
use crate::enums::tls_error::TLSError;

const SERVER_NAME: u16 = 0;
const MAX_FRAGMENT_LENGTH: u16 = 1;
const STATUS_REQUEST: u16 = 5;
const SUPPORTED_GROUPS: u16 = 10;
const SIGNATURE_ALGORITHMS: u16 = 13;
const USE_SRTP: u16 = 14;
const HEARTBEAT: u16 = 15;
const APPLICATION_LAYER_PROTOCOL_NEGOTIATION: u16 = 16;
const SIGNED_CERTIFICATE_TIMESTAMP: u16 = 18;
const CLIENT_CERTIFICATE_TYPE: u16 = 19;
const SERVER_CERTIFICATE_TYPE: u16 = 20;
const PADDING: u16 = 21;
const PRE_SHARED_KEY: u16 = 41;
const EARLY_DATA: u16 = 42;
const SUPPORTED_VERSIONS: u16 = 43;
const COOKIE: u16 = 44;
const PSK_KEY_EXCHANGE_MODES: u16 = 45;
const CERTIFICATE_AUTHORITIES: u16 = 47;
const OID_FILTERS: u16 = 48;
const POST_HANDSHAKE_AUTH: u16 = 49;
const SIGNATURE_ALGORITHMS_CERT: u16 = 50;
const KEY_SHARE: u16 = 51;

#[derive(Debug, PartialOrd, PartialEq, Clone)]
pub enum ExtensionType{
    ServerName, /* 0 RFC 6066 */
    MaxFragmentLength, /* 1 RFC 6066 */
    StatusRequest, /* 5 RFC 6066 */
    SupportedGroups, /* 10 RFC 8422, 7919 */
    SignatureAlgorithms, /* 13 RFC 8446 */
    UseSrtp, /* 14 RFC 5764 */
    Heartbeat, /* 15 RFC 6520 */
    ApplicationLayerProtocolNegotiation, /* 16 RFC 7301 */
    SignedCertificateTimestamp, /* 18 RFC 6962 */
    ClientCertificateType, /* 19 RFC 7250 */
    ServerCertificateType, /* 20 RFC 7250 */
    Padding, /* 21 RFC 7685 */
    PreSharedKey, /* 41 RFC 8446 */
    EarlyData, /* 42 RFC 8446 */
    SupportedVersions, /*43 RFC 8446 */
    Cookie, /* 44 RFC 8446 */
    PskKeyExchangeModes, /* 45 RFC 8446 */
    CertificateAuthorities, /* 47 RFC 8446 */
    OidFilters, /* 48 RFC 8446 */
    PostHandshakeAuth, /* 49 RFC 8446 */
    SignatureAlgorithmsCert, /* 50 RFC 8446 */
    KeyShare, /* 51 RFC 8446 */
    Unknown(u16),
}

impl<'a> DecodeFrom<'a> for ExtensionType {
    fn decode(bytes: &[u8]) -> Result<(ExtensionType, usize, &[u8]), TLSError> {
        let value = match transmute::u16_from_bytes_be(&bytes[..2]) {
            SERVER_NAME => ExtensionType::ServerName,
            MAX_FRAGMENT_LENGTH => ExtensionType::MaxFragmentLength,
            STATUS_REQUEST => ExtensionType::StatusRequest,
            SUPPORTED_GROUPS => ExtensionType::SupportedGroups,
            SIGNATURE_ALGORITHMS => ExtensionType::SignatureAlgorithms,
            USE_SRTP => ExtensionType::UseSrtp,
            HEARTBEAT => ExtensionType::Heartbeat,
            APPLICATION_LAYER_PROTOCOL_NEGOTIATION => ExtensionType::ApplicationLayerProtocolNegotiation,
            SIGNED_CERTIFICATE_TIMESTAMP => ExtensionType::SignedCertificateTimestamp,
            CLIENT_CERTIFICATE_TYPE => ExtensionType::ClientCertificateType,
            SERVER_CERTIFICATE_TYPE => ExtensionType::ServerCertificateType,
            PADDING => ExtensionType::Padding,
            PRE_SHARED_KEY => ExtensionType::PreSharedKey,
            EARLY_DATA => ExtensionType::EarlyData,
            SUPPORTED_VERSIONS => ExtensionType::SupportedVersions,
            COOKIE => ExtensionType::Cookie,
            PSK_KEY_EXCHANGE_MODES => ExtensionType::PskKeyExchangeModes,
            CERTIFICATE_AUTHORITIES => ExtensionType::CertificateAuthorities,
            OID_FILTERS => ExtensionType::OidFilters,
            POST_HANDSHAKE_AUTH => ExtensionType::PostHandshakeAuth,
            SIGNATURE_ALGORITHMS_CERT => ExtensionType::SignatureAlgorithmsCert,
            KEY_SHARE => ExtensionType::KeyShare,
            value => ExtensionType::Unknown(value),
        };
        Ok((value, 2, &bytes[2..]))
    }
}
impl EncodeTo for ExtensionType {
    fn encode_to(&self, to:&mut Vec<u8>) {
        let value = match self {
            ExtensionType::ServerName => SERVER_NAME,
            ExtensionType::MaxFragmentLength => MAX_FRAGMENT_LENGTH,
            ExtensionType::StatusRequest => STATUS_REQUEST,
            ExtensionType::SupportedGroups => SUPPORTED_GROUPS,
            ExtensionType::SignatureAlgorithms => SIGNATURE_ALGORITHMS,
            ExtensionType::UseSrtp => USE_SRTP,
            ExtensionType::Heartbeat => HEARTBEAT,
            ExtensionType::ApplicationLayerProtocolNegotiation => APPLICATION_LAYER_PROTOCOL_NEGOTIATION,
            ExtensionType::SignedCertificateTimestamp => SIGNED_CERTIFICATE_TIMESTAMP,
            ExtensionType::ClientCertificateType => CLIENT_CERTIFICATE_TYPE,
            ExtensionType::ServerCertificateType => SERVER_CERTIFICATE_TYPE,
            ExtensionType::Padding => PADDING,
            ExtensionType::PreSharedKey => PRE_SHARED_KEY,
            ExtensionType::EarlyData => EARLY_DATA,
            ExtensionType::SupportedVersions => SUPPORTED_VERSIONS,
            ExtensionType::Cookie => COOKIE,
            ExtensionType::PskKeyExchangeModes => PSK_KEY_EXCHANGE_MODES,
            ExtensionType::CertificateAuthorities => CERTIFICATE_AUTHORITIES,
            ExtensionType::OidFilters => OID_FILTERS,
            ExtensionType::PostHandshakeAuth => POST_HANDSHAKE_AUTH,
            ExtensionType::SignatureAlgorithmsCert => SIGNATURE_ALGORITHMS_CERT,
            ExtensionType::KeyShare => KEY_SHARE,
            ExtensionType::Unknown(value) => *value,
        };
        to.extend_from_slice(&transmute::u16_to_bytes_be(value));
    }
}

