use crate::structs::key_share_entry::KeyShareEntry;
use crate::enums::protocol_version::ProtocolVersion;
use crate::enums::named_group::NamedGroup;
use crate::enums::signature_scheme::SignatureScheme;
use crate::enums::protocol_name::ProtocolName;
use crate::enums::psk_key_exchange_mode::PskKeyExchangeMode;
use crate::structs::server_name::ServerName;
use crate::utils::transmute;
use crate::enums::tls_error::TLSError;
use crate::enums::extension_type::ExtensionType;
use crate::utils::transmute::EncodeTo;
use crate::utils::transmute::DecodeFrom;
use crate::structs::certificate_status_request::CertificateStatusRequest;
use crate::structs::certificate_status::CertificateStatus;

#[derive(Debug)]
pub enum Extension {
    KeyShareClientHello{ client_shares: Box < [KeyShareEntry] >},
    KeyShareServerHello(KeyShareEntry),
    SupportedVersionsClientHello(Box<[ProtocolVersion]>),
    SupportedVersionsServerHello { selected_version: ProtocolVersion },
    SupportedGroups(Box<[NamedGroup]>),
    SignatureAlgorithms { supported_signature_algorithms: Box<[SignatureScheme]>},
    ApplicationLayerProtocolNegotiation(Box<[ProtocolName]>),
    PskKeyExchangeModes { ke_modes: Box<[PskKeyExchangeMode]>},
    ServerName { server_name_list: Box<[ServerName]>},
    Padding(usize),
    CertificateStatusRequest(CertificateStatusRequest),
    CertificateStatus(CertificateStatus),
    Unsupported(ExtensionType, Box<[u8]>),
}

impl EncodeTo for Extension {
    fn encode_to(&self, to: &mut Vec<u8>) {
        let mut extension_vec: Vec<u8> = Vec::new();
        match self {
            Extension::KeyShareServerHello(server_share) => {
                ExtensionType::KeyShare.encode_to(to);
                server_share.encode_to(&mut extension_vec);
            },
            Extension::KeyShareClientHello { client_shares } => {
                ExtensionType::KeyShare.encode_to(to);
                transmute::encode_list_to(&mut extension_vec, client_shares, 2);
            },
            Extension::SupportedVersionsServerHello { selected_version } => {
                ExtensionType::SupportedVersions.encode_to(to);
                selected_version.encode_to(&mut extension_vec);
            },
            Extension::SupportedVersionsClientHello(versions) => {
                ExtensionType::SupportedVersions.encode_to(to);
                transmute::encode_list_to(&mut extension_vec, versions, 1);
            },
            Extension::SupportedGroups(named_group_list) => {
                ExtensionType::SupportedGroups.encode_to(to);
                transmute::encode_list_to(&mut extension_vec, named_group_list, 2);
            },
            Extension::ApplicationLayerProtocolNegotiation(protocol_name_list) => {
                ExtensionType::ApplicationLayerProtocolNegotiation.encode_to(to);
                transmute::encode_list_to(&mut extension_vec, protocol_name_list, 2);
            },
            Extension::SignatureAlgorithms { supported_signature_algorithms } => {
                ExtensionType::SignatureAlgorithms.encode_to(to);
                transmute::encode_list_to(&mut extension_vec, supported_signature_algorithms, 2)
            },
            Extension::PskKeyExchangeModes { ke_modes } => {
                ExtensionType::PskKeyExchangeModes.encode_to(to);
                transmute::encode_list_to(&mut extension_vec, ke_modes, 1);
            },
            Extension::Padding(length) => {
                ExtensionType::Padding.encode_to(to);
                let padding = transmute::create_buffer(*length);
                extension_vec.extend_from_slice(&padding);
            },
            Extension::ServerName{ server_name_list} => {
                ExtensionType::ServerName.encode_to(to);
                transmute::encode_list_to(&mut extension_vec, server_name_list, 2);
            },
            Extension::CertificateStatusRequest(certificate_status_request) => {
                ExtensionType::StatusRequest.encode_to(to);
                certificate_status_request.encode_to(&mut extension_vec);
            },
            Extension::CertificateStatus(certificate_status) => {
                ExtensionType::StatusRequest.encode_to(to);
                certificate_status.encode_to(&mut extension_vec);
            },
            Extension::Unsupported(extension_type, data) => {
                extension_type.encode_to(to);
                extension_vec.extend_from_slice(&data);
            },
        }

        transmute::encode_slice_to(to, &extension_vec, 2);
    }
}

impl<'a> DecodeFrom<'a> for Extension {
    fn decode(bytes: &'a [u8]) -> Result<(Extension, usize, &'a [u8]), TLSError> {
        let (extension_type, type_size, bytes) = ExtensionType::decode(bytes)?;
        let (extension_data, data_size, bytes) = transmute::decode_slice(bytes, 2);
        let extension = match extension_type {
            ExtensionType::ApplicationLayerProtocolNegotiation => {
                let (protocol_name_list, _, _) = transmute::decode_list(extension_data, 2)?;
                Extension::ApplicationLayerProtocolNegotiation(protocol_name_list)
            }
            ExtensionType::PskKeyExchangeModes => {
                let (ke_modes, _, _) = transmute::decode_list(extension_data, 1)?;
                Extension::PskKeyExchangeModes {ke_modes}
            }
            ExtensionType::SignatureAlgorithms => {
                let (supported_signature_algorithms, _, _) = transmute::decode_list(extension_data, 2)?;
                Extension::SignatureAlgorithms {supported_signature_algorithms}
            }
            ExtensionType::Padding => Extension::Padding(extension_data.len()),
            ExtensionType::ServerName => {
                let (server_name_list, _, _) = transmute::decode_list(extension_data, 2)?;
                Extension::ServerName {server_name_list}
            }
            ExtensionType::SupportedGroups => {
                let (named_group_list, _, _) = transmute::decode_list(extension_data, 2)?;
                Extension::SupportedGroups(named_group_list)
            },
            extension_type => {
                Extension::Unsupported(extension_type, Box::from(extension_data))
            }
        };

        Ok((extension, type_size + data_size, bytes))
    }
}

impl Extension {

    pub fn decode_client_extension(bytes: &[u8]) -> Result<(Extension, usize, &[u8]), TLSError> {
        let origin = bytes;
        let (extension_type, type_size, bytes) = ExtensionType::decode(bytes)?;
        let (extension_data, data_size, bytes) = transmute::decode_slice(bytes, 2);
        let extension = match extension_type {
            ExtensionType::SupportedVersions => {
                let (versions, _, _) = transmute::decode_list(extension_data, 1)?;
                Extension::SupportedVersionsClientHello(versions)
            },
            ExtensionType::KeyShare => {
                let (client_shares, _, _) = transmute::decode_list(extension_data, 2)?;
                Extension::KeyShareClientHello {client_shares}
            },
            ExtensionType::StatusRequest => {
                let (certificate_status_request, _, _) = CertificateStatusRequest::decode(extension_data)?;
                Extension::CertificateStatusRequest(certificate_status_request)
            },
            _ => {
                let (extension, _, _)  = Self::decode(origin)?;
                extension
            },
        };

        Ok((extension, type_size + data_size, bytes))
    }

    pub fn decode_server_extension(bytes: &[u8]) -> Result<(Extension, usize, &[u8]), TLSError> {
        let origin = bytes;
        let (extension_type, type_size, bytes) = ExtensionType::decode(bytes)?;
        let (extension_data, data_size, bytes) = transmute::decode_slice(bytes, 2);

        let extension = match extension_type {
            ExtensionType::SupportedVersions => {
                let (protocol_version, _, _) = ProtocolVersion::decode(extension_data)?;
                Extension::SupportedVersionsServerHello{selected_version: protocol_version}
            }
            ExtensionType::KeyShare => {
                let (server_share, _, _) = KeyShareEntry::decode(extension_data)?;
                Extension::KeyShareServerHello(server_share)
            }
            _ => {
                let (extension, _, _) = Self::decode(origin)?;
                extension
            },
        };

        Ok((extension, type_size + data_size, bytes))
    }

    pub fn decode_certificate_extension(bytes: &[u8]) -> Result<(Extension, usize, &[u8]), TLSError> {
        let origin = bytes;
        let (extension_type, type_size, bytes) = ExtensionType::decode(bytes)?;
        let (extension_data, data_size, bytes) = transmute::decode_slice(bytes, 2);

        let extension = match extension_type {
            ExtensionType::StatusRequest => {
                let (certificate_status, _, _) = CertificateStatus::decode(extension_data)?;
                Extension::CertificateStatus(certificate_status)
            },
            _ => {
                let (extension, _, _) = Self::decode(origin)?;
                extension
            },
        };

        Ok((extension, type_size + data_size, bytes))
    }
}