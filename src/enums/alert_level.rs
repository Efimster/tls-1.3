use crate::enums::tls_error::TLSError;
use crate::enums::alert_description::AlertDescription;

const WARNING:u8 = 1;
const FATAL: u8 = 2;

#[derive(Debug)]
pub enum AlertLevel {
    Warning, // 1
    Fatal, // 2
}

impl AlertLevel {
    pub fn decode(byte: u8) -> Result<AlertLevel, TLSError> {
        match byte {
            WARNING => Ok(AlertLevel::Warning),
            FATAL => Ok(AlertLevel::Fatal),
            _ => Err(TLSError::Alert(AlertDescription::DecodeError)),
        }
    }
}