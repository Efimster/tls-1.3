use crate::enums::tls_error::TLSError;
use crate::enums::alert_description::AlertDescription;
use crate::utils::transmute::EncodeTo;
use crate::utils::transmute::DecodeFrom;

const CLIENT_HELLO:u8 = 1;
const SERVER_HELLO: u8 = 2;
const NEW_SESSION_TICKET: u8 = 4;
const END_OF_EARLY_DATA: u8 = 5;
const ENCRYPTED_EXTENSIONS: u8 = 8;
const CERTIFICATE: u8 = 11;
const CERTIFICATE_REQUEST: u8 = 13;
const CERTIFICATE_VERIFY: u8 = 15;
const FINISHED: u8 = 20;
const KEY_UPDATE: u8 = 24;
const MESSAGE_HASH: u8 = 254;


#[derive(Debug, PartialOrd, PartialEq, Clone, Copy)]
pub enum HandshakeType {
    ClientHello, // 1
    ServerHello, // 2
    NewSessionTicket, // 4
    EndOfEarlyData, // 5
    EncryptedExtensions, // 8
    Certificate, // 11
    CertificateRequest, // 13
    CertificateVerify, // 15
    Finished, // 20
    KeyUpdate, // 24
    MessageHash, // 254
}

impl<'a> DecodeFrom<'a> for HandshakeType {
    fn decode(bytes:&[u8]) -> Result<(HandshakeType, usize, &[u8]), TLSError>{
        let handshake_type = match bytes[0] {
            CLIENT_HELLO => HandshakeType::ClientHello,
            SERVER_HELLO => HandshakeType::ServerHello,
            NEW_SESSION_TICKET => HandshakeType::NewSessionTicket,
            END_OF_EARLY_DATA => HandshakeType::EndOfEarlyData,
            ENCRYPTED_EXTENSIONS => HandshakeType::EncryptedExtensions,
            CERTIFICATE => HandshakeType::Certificate,
            CERTIFICATE_REQUEST => HandshakeType::CertificateRequest,
            CERTIFICATE_VERIFY => HandshakeType::CertificateVerify,
            FINISHED => HandshakeType::Finished,
            KEY_UPDATE => HandshakeType::KeyUpdate,
            MESSAGE_HASH => HandshakeType::MessageHash,
            _ => return Err(TLSError::Alert(AlertDescription::DecodeError)),
        };
        Ok((handshake_type, 1, &bytes[1..]))
    }
}

impl EncodeTo for HandshakeType {
    fn encode_to(&self, to: &mut Vec<u8>) {
        let value = match self {
            HandshakeType::ClientHello => CLIENT_HELLO,
            HandshakeType::ServerHello => SERVER_HELLO,
            HandshakeType::NewSessionTicket => NEW_SESSION_TICKET,
            HandshakeType::EndOfEarlyData => END_OF_EARLY_DATA,
            HandshakeType::EncryptedExtensions => ENCRYPTED_EXTENSIONS,
            HandshakeType::Certificate => CERTIFICATE,
            HandshakeType::CertificateRequest => CERTIFICATE_REQUEST,
            HandshakeType::CertificateVerify => CERTIFICATE_VERIFY,
            HandshakeType::Finished => FINISHED,
            HandshakeType::KeyUpdate => KEY_UPDATE,
            HandshakeType::MessageHash => MESSAGE_HASH,
        };
        to.push(value);
    }
}