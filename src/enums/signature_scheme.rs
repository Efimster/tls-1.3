use crate::utils::transmute;
use crate::utils::transmute::EncodeTo;
use crate::utils::transmute::DecodeFrom;
use crate::enums::tls_error::TLSError;
use crate::enums::alert_description::AlertDescription;

const RSA_PKCS_1_SHA_256:u16 = 0x0401;
const RSA_PKCS_1_SHA_384: u16 = 0x0501;
const RSA_PKCS_1_SHA_512: u16 = 0x0601;
const ECDSA_SECP_256_R1_SHA_256: u16 = 0x0403;
const ECDSA_SECP_256_R1_SHA_384: u16 = 0x0503;
const ECDSA_SECP_256_R1_SHA_512: u16 = 0x0603;
const RSA_PSS_RSAE_SHA_256: u16 = 0x0804;
const RSA_PSS_RSAE_SHA_384: u16 = 0x0805;
const RSA_PSS_RSAE_SHA_512: u16 = 0x0806;
const ED_25519: u16 = 0x0807;
const ED_448: u16 = 0x0808;
const RSA_PSS_PSS_SHA_256: u16 = 0x0809;
const RSA_PSS_PSS_SHA_384: u16 = 0x080a;
const RSA_PSS_PSS_SHA_512: u16 = 0x080b;
const RSA_PKCS_1_SHA_1: u16 = 0x0201;
const ECDSA_SHA_1: u16 = 0x0203;
const DSA_SHA1_RESERVED: u16 = 0x0202;
const DSA_SHA256_RESERVED: u16 = 0x0402;
const DSA_SHA384_RESERVED: u16 = 0x0502;
const DSA_SHA512_RESERVED: u16 = 0x0602;



#[derive(Debug, Clone, Copy)]
pub enum SignatureScheme {
/* RSASSA-PKCS1-v1_5 algorithms */
    RsaPkcs1Sha256, //(0x0401),
    RsaPkcs1Sha384, //(0x0501),
    RsaPkcs1Sha512, //(0x0601),
/* ECDSA algorithms */
    EcdsaSecp256r1Sha256, //(0x0403),
    EcdsaSecp384r1Sha384, //(0x0503),
    EcdsaSecp521r1Sha512, //(0x0603),
/* RSASSA-PSS algorithms with public key OID rsaEncryption */
    RsaPssRsaeSha256, //(0x0804),
    RsaPssRsaeSha384, //(0x0805),
    RsaPssRsaeSha512, //(0x0806),
/* EdDSA algorithms */
    Ed25519, //(0x0807),
    Ed448, //(0x0808),
/* RSASSA-PSS algorithms with public key OID RSASSA-PSS */
    RsaPssPssSha256, //(0x0809),
    RsaPssPssSha384, //(0x080a),
    RsaPssPssSha512, //(0x080b),
/* Legacy algorithms */
    RsaPkcs1Sha1, //(0x0201),
    EcdsaSha1, //(0x0203),
/* Reserved Code Points */
    ObsoleteRESERVED(u16), //(0x0000..0x0200), (0x0204..0x0400), (0x0404..0x0500), (0x0504..0x0600), (0x0604..0x06FF)
    DsaSha1RESERVED, //(0x0202),
    DsaSha256RESERVED, //(0x0402),
    DsaSha384RESERVED, //(0x0502),
    DsaSha512RESERVED, //(0x0602),
    PrivateUse(u16), //(0xFE00..0xFFFF),
}

impl<'a> DecodeFrom<'a> for SignatureScheme {
    fn decode(bytes: &[u8]) -> Result<(SignatureScheme, usize, &[u8]), TLSError> {
        let value:u16 = ((bytes[0] as u16)  << 8) | bytes[1] as u16;
        let scheme = match value {
            RSA_PKCS_1_SHA_256 => SignatureScheme::RsaPkcs1Sha256,
            RSA_PKCS_1_SHA_384 => SignatureScheme::RsaPkcs1Sha384,
            RSA_PKCS_1_SHA_512 => SignatureScheme::RsaPkcs1Sha512,
            ECDSA_SECP_256_R1_SHA_256 => SignatureScheme::EcdsaSecp256r1Sha256,
            ECDSA_SECP_256_R1_SHA_384 => SignatureScheme::EcdsaSecp384r1Sha384,
            ECDSA_SECP_256_R1_SHA_512 => SignatureScheme::EcdsaSecp521r1Sha512,
            RSA_PSS_RSAE_SHA_256 => SignatureScheme::RsaPssRsaeSha256,
            RSA_PSS_RSAE_SHA_384 => SignatureScheme::RsaPssRsaeSha384,
            RSA_PSS_RSAE_SHA_512 => SignatureScheme::RsaPssRsaeSha512,
            ED_25519 => SignatureScheme::Ed25519,
            ED_448 => SignatureScheme::Ed448,
            RSA_PSS_PSS_SHA_256 => SignatureScheme::RsaPssPssSha256,
            RSA_PSS_PSS_SHA_384 => SignatureScheme::RsaPssPssSha384,
            RSA_PSS_PSS_SHA_512 => SignatureScheme::RsaPssPssSha512,
            RSA_PKCS_1_SHA_1 => SignatureScheme::RsaPkcs1Sha1,
            ECDSA_SHA_1 => SignatureScheme::EcdsaSha1,
            value @ 0x0000..=0x0200
                | value @ 0x0204..=0x0400
                | value @ 0x0404..=0x0500
                | value @ 0x0504..=0x0600
                | value @ 0x0604..=0x06FF => SignatureScheme::ObsoleteRESERVED(value),
            DSA_SHA1_RESERVED => SignatureScheme::DsaSha1RESERVED,
            DSA_SHA256_RESERVED => SignatureScheme::DsaSha256RESERVED,
            DSA_SHA384_RESERVED => SignatureScheme::DsaSha384RESERVED,
            DSA_SHA512_RESERVED => SignatureScheme::DsaSha512RESERVED,
            value @ 0xFE00..=0xFFFF => SignatureScheme::PrivateUse(value),
            _ => return Err(TLSError::Alert(AlertDescription::DecodeError)),
        };
        Ok((scheme, 2, &bytes[2 ..]))
    }
}

impl EncodeTo for SignatureScheme {
    fn encode_to(&self, to: &mut Vec<u8>){
        let value = match self {
            SignatureScheme::RsaPkcs1Sha256 => RSA_PKCS_1_SHA_256,
            SignatureScheme::RsaPkcs1Sha384 => RSA_PKCS_1_SHA_384,
            SignatureScheme::RsaPkcs1Sha512 => RSA_PKCS_1_SHA_512,
            SignatureScheme::EcdsaSecp256r1Sha256 => ECDSA_SECP_256_R1_SHA_256,
            SignatureScheme::EcdsaSecp384r1Sha384 => ECDSA_SECP_256_R1_SHA_384,
            SignatureScheme::EcdsaSecp521r1Sha512 => ECDSA_SECP_256_R1_SHA_512,
            SignatureScheme::RsaPssRsaeSha256 => RSA_PSS_RSAE_SHA_256,
            SignatureScheme::RsaPssRsaeSha384 => RSA_PSS_RSAE_SHA_384,
            SignatureScheme::RsaPssRsaeSha512 => RSA_PSS_RSAE_SHA_512,
            SignatureScheme::Ed25519 => ED_25519,
            SignatureScheme::Ed448 => ED_448,
            SignatureScheme::RsaPssPssSha256 => RSA_PSS_PSS_SHA_256,
            SignatureScheme::RsaPssPssSha384 => RSA_PSS_PSS_SHA_384,
            SignatureScheme::RsaPssPssSha512 => RSA_PSS_PSS_SHA_512,
            SignatureScheme::RsaPkcs1Sha1 => RSA_PKCS_1_SHA_1,
            SignatureScheme::EcdsaSha1 => ECDSA_SHA_1,
            SignatureScheme::ObsoleteRESERVED(value) => *value,
            SignatureScheme::DsaSha1RESERVED => DSA_SHA1_RESERVED,
            SignatureScheme::DsaSha256RESERVED => DSA_SHA256_RESERVED,
            SignatureScheme::DsaSha384RESERVED => DSA_SHA384_RESERVED,
            SignatureScheme::DsaSha512RESERVED => DSA_SHA512_RESERVED,
            SignatureScheme::PrivateUse(value) => *value,
        };

        to.extend_from_slice(&transmute::u16_to_bytes_be(value));
    }
}