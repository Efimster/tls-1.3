use crate::enums::tls_error::TLSError;

const CLOSE_NOTIFY:u8 = 0;
const UNEXPECTED_MESSAGE: u8 = 10;
const BAD_RECORD_MAC: u8 = 20;
const RECORD_OVERFLOW: u8 = 22;
const HANDSHAKE_FAILURE: u8 = 40;
const BAD_CERTIFICATE: u8 = 42;
const UNSUPPORTED_CERTIFICATE: u8 = 43;
const CERTIFICATE_REVOKED: u8 = 44;
const CERTIFICATE_EXPIRED: u8 = 45;
const CERTIFICATE_UNKNOWN: u8 = 46;
const ILLEGAL_PARAMETER: u8 = 47;
const UNKNOWN_CA: u8 = 48;
const ACCESS_DENIED: u8 = 49;
const DECODE_ERROR: u8 = 50;
const DECRYPT_ERROR: u8 = 51;
const PROTOCOL_VERSION: u8 = 70;
const INSUFFICIENT_SECURITY: u8 = 71;
const INTERNAL_ERROR: u8 = 80;
const INAPPROPRIATE_FALLBACK: u8 = 86;
const USER_CANCELED: u8 = 90;
const MISSING_EXTENSION: u8 = 109;
const UNSUPPORTED_EXTENSION: u8 = 110;
const UNRECOGNIZED_NAME: u8 = 112;
const BAD_CERTIFICATE_STATUS_RESPONSE: u8 = 113;
const UNKNOWN_PSK_IDENTITY: u8 = 115;
const CERTIFICATE_REQUIRED: u8 = 116;
const NO_APPLICATION_PROTOCOL: u8 = 120;

#[derive(Debug, Clone, PartialOrd, PartialEq)]
pub enum AlertDescription {
    CloseNotify, // 0
    UnexpectedMessage, // (10)
    BadRecordMac, // (20)
    DecryptionFailedRESERVED, // (21)
    RecordOverflow, // (22),
    DecompressionFailureRESERVED, //(30),
    HandshakeFailure, //(40),
    NoCertificateRESERVED, //(41),
    BadCertificate, //(42),
    UnsupportedCertificate, //(43),
    CertificateRevoked, //(44),
    CertificateExpired, //(45),
    CertificateUnknown, //(46),
    IllegalParameter, //(47),
    UnknownCa, //(48),
    AccessDenied, //(49),
    DecodeError, //(50),
    DecryptError, //(51),
    ExportRestrictionRESERVED, //(60),
    ProtocolVersion, //(70),
    InsufficientSecurity, //(71),
    InternalError, //(80),
    InappropriateFallback, //(86),
    UserCanceled, //(90),
    NoRenegotiationRESERVED, //(100),
    MissingExtension, //(109),
    UnsupportedExtension, //(110),
    CertificateUnobtainableRESERVED, //(111),
    UnrecognizedName, //(112),
    BadCertificateStatusResponse, //(113),
    BadCertificateHashValueRESERVED, //(114),
    UnknownPskIdentity, //(115),
    CertificateRequired, //(116),
    NoApplicationProtocol, //(120),
    Received(Box<AlertDescription>),

    SocketReadTimeout,
}

impl AlertDescription {
    pub fn decode(byte: u8) -> Result<AlertDescription, TLSError> {
        match byte {
            CLOSE_NOTIFY => Ok(AlertDescription::CloseNotify),
            UNEXPECTED_MESSAGE => Ok(AlertDescription::UnexpectedMessage),
            BAD_RECORD_MAC => Ok(AlertDescription::BadRecordMac),
            RECORD_OVERFLOW => Ok(AlertDescription::RecordOverflow),
            HANDSHAKE_FAILURE => Ok(AlertDescription::HandshakeFailure),
            BAD_CERTIFICATE => Ok(AlertDescription::BadCertificate),
            UNSUPPORTED_CERTIFICATE => Ok(AlertDescription::UnsupportedCertificate),
            CERTIFICATE_REVOKED => Ok(AlertDescription::CertificateRevoked),
            CERTIFICATE_EXPIRED => Ok(AlertDescription::CertificateExpired),
            CERTIFICATE_UNKNOWN => Ok(AlertDescription::CertificateUnknown),
            ILLEGAL_PARAMETER => Ok(AlertDescription::IllegalParameter),
            UNKNOWN_CA => Ok(AlertDescription::UnknownCa),
            ACCESS_DENIED => Ok(AlertDescription::AccessDenied),
            DECODE_ERROR => Ok(AlertDescription::DecodeError),
            DECRYPT_ERROR => Ok(AlertDescription::DecryptError),
            PROTOCOL_VERSION => Ok(AlertDescription::ProtocolVersion),
            INSUFFICIENT_SECURITY => Ok(AlertDescription::InsufficientSecurity),
            INTERNAL_ERROR => Ok(AlertDescription::InternalError),
            INAPPROPRIATE_FALLBACK => Ok(AlertDescription::InappropriateFallback),
            USER_CANCELED => Ok(AlertDescription::UserCanceled),
            MISSING_EXTENSION => Ok(AlertDescription::MissingExtension),
            UNSUPPORTED_EXTENSION => Ok(AlertDescription::UnsupportedExtension),
            UNRECOGNIZED_NAME => Ok(AlertDescription::UnrecognizedName),
            BAD_CERTIFICATE_STATUS_RESPONSE => Ok(AlertDescription::BadCertificateStatusResponse),
            UNKNOWN_PSK_IDENTITY => Ok(AlertDescription::UnknownPskIdentity),
            CERTIFICATE_REQUIRED => Ok(AlertDescription::CertificateRequired),
            NO_APPLICATION_PROTOCOL => Ok(AlertDescription::NoApplicationProtocol),
            _ => Err(TLSError::Alert(AlertDescription::DecodeError)),
        }
    }
}