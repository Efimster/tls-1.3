use std::fmt;
use std::io;
use crate::enums::alert_description::AlertDescription;
use std::string::FromUtf8Error;
use std::str::Utf8Error;
use x509::x509_error::X509Error;
use std::io::ErrorKind;

#[derive(Clone, Debug)]
pub enum TLSError {
    Alert(AlertDescription),
    IO(ErrorKind),
}

impl TLSError {
//    pub fn from_alert(alert_description: AlertDescription) -> TLSError{
//        TLSError {description: alert_description}
//    }
}

impl fmt::Display for TLSError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            TLSError::Alert(description) => write!(f, "Alert {:?}", description),
            TLSError::IO(kind) => write!(f, "IO {:?}", kind),
        }
    }
}

impl From<io::Error> for TLSError {
    fn from(error: io::Error) -> Self {
        TLSError::IO(error.kind())
    }
}

impl From<FromUtf8Error> for TLSError {
    fn from(_: FromUtf8Error) -> Self {
        TLSError::Alert(AlertDescription::DecodeError)
    }
}

impl From<AlertDescription> for TLSError {
    fn from(description: AlertDescription) -> Self {
        TLSError::Alert(description)
    }
}

impl From<Utf8Error> for TLSError {
    fn from(_: Utf8Error) -> Self {
        TLSError::Alert(AlertDescription::DecodeError)
    }
}

impl From<X509Error> for TLSError {
    fn from(error: X509Error) -> Self {
        let description = match error {
            X509Error::Sign => AlertDescription::BadCertificate,
            _ => AlertDescription::DecodeError,
        };

        TLSError::Alert(description)
    }
}

impl From<TLSError> for io::Error {
    fn from(error: TLSError) -> Self {
        match error {
            TLSError::IO(kind) => kind.into(),
            _ => io::ErrorKind::Other.into(),
        }
    }
}