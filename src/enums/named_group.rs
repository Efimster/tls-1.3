use crate::utils::transmute;
use crate::utils::transmute::EncodeTo;
use crate::enums::tls_error::TLSError;
use crate::utils::transmute::DecodeFrom;

const SECP256R1: u16 = 0x0017;
const SECP384R1: u16 = 0x0018;
const SECP521R1: u16 = 0x0019;
const X25519: u16 = 0x001D;
const X448: u16 = 0x001E;
const FFDHE2048: u16 = 0x0100;
const FFDHE3072: u16 = 0x0101;
const FFDHE4096: u16 = 0x0102;
const FFDHE6144: u16 = 0x0103;
const FFDHE8192: u16 = 0x0104;



#[derive(Debug, PartialEq, PartialOrd)]
pub enum NamedGroup {
    /* Elliptic Curve Groups (ECDHE) */
    Secp256r1, //(0x0017),
    Secp384r1, //(0x0018),
    Secp521r1, //(0x0019),
    X25519, //(0x001D),
    X448, //(0x001E),
    /* Finite Field Groups (DHE) */
    Ffdhe2048, //(0x0100),
    Ffdhe3072, //(0x0101),
    Ffdhe4096, //(0x0102),
    Ffdhe6144, //(0x0103),
    Ffdhe8192, //(0x0104),
    Private(u16),
}

impl<'a> DecodeFrom<'a> for NamedGroup {
    fn decode(bytes:&[u8]) -> Result<(NamedGroup, usize, &[u8]), TLSError> {
        let value = match transmute::u16_from_bytes_be(bytes) {
            SECP256R1 => NamedGroup::Secp256r1,
            SECP384R1 => NamedGroup::Secp384r1,
            SECP521R1 => NamedGroup::Secp521r1,
            X25519 => NamedGroup::X25519,
            X448 => NamedGroup::X448,
            FFDHE2048 => NamedGroup::Ffdhe2048,
            FFDHE3072 => NamedGroup::Ffdhe3072,
            FFDHE4096 => NamedGroup::Ffdhe4096,
            FFDHE6144 => NamedGroup::Ffdhe6144,
            FFDHE8192 => NamedGroup::Ffdhe8192,
            value @ _ => NamedGroup::Private(value),
        };
        Ok((value, 2, &bytes[2..]))
    }
}

impl EncodeTo for NamedGroup {
    fn encode_to(&self, to: &mut Vec<u8>) {
        let value = match self {
            NamedGroup::Secp256r1 => SECP256R1,
            NamedGroup::Secp384r1 => SECP384R1,
            NamedGroup::Secp521r1 => SECP521R1,
            NamedGroup::X25519 => X25519,
            NamedGroup::X448 => X448,
            NamedGroup::Ffdhe2048 => FFDHE2048,
            NamedGroup::Ffdhe3072 => FFDHE3072,
            NamedGroup::Ffdhe4096 => FFDHE4096,
            NamedGroup::Ffdhe6144 => FFDHE6144,
            NamedGroup::Ffdhe8192 => FFDHE8192,
            NamedGroup::Private(value) => *value,
        };

        to.extend_from_slice(&transmute::u16_to_bytes_be(value));
    }
}