use crate::structs::client_hello::ClientHello;
use crate::structs::server_hello::ServerHello;
use crate::structs::encrypted_extensions::EncryptedExtensions;
use crate::enums::handshake_type::HandshakeType;
use crate::utils::transmute;
use crate::enums::tls_error::TLSError;
use crate::utils::transmute::EncodeTo;
use crate::structs::finished::Finished;
use crate::structs::certificate_verify::CertificateVerify;
use crate::structs::certificate::Certificate;
use crate::utils::transmute::DecodeFrom;
use crate::structs::new_session_ticket::NewSessionTicket;

#[derive(Debug)]
pub enum Handshake {
    ClientHello (ClientHello),
    ServerHello (ServerHello),
    NewSessionTicket(NewSessionTicket),
    EncryptedExtensions(EncryptedExtensions),
    Certificate (Certificate),
    CertificateVerify (CertificateVerify),
    Finished (Finished),
    Unsupported (HandshakeType, Box<[u8]>)
}

impl EncodeTo for Handshake {
    fn encode_to(&self, to: &mut Vec<u8>) {
        let mut message_vec: Vec<u8> = Vec::new();
        match self {
            Handshake::ServerHello(server_hello) => {
                HandshakeType::ServerHello.encode_to(to);
                server_hello.encode_to(&mut message_vec);
            },
            Handshake::EncryptedExtensions(encrypted_extensions) => {
                HandshakeType::EncryptedExtensions.encode_to(to);
                encrypted_extensions.encode_to(&mut message_vec);
            },
            Handshake::ClientHello(client_hello) => {
                HandshakeType::ClientHello.encode_to(to);
                client_hello.encode_to(&mut message_vec);
            },
            Handshake::Certificate(certificate) => {
                HandshakeType::Certificate.encode_to(to);
                certificate.encode_to(&mut message_vec);
            },
            Handshake::CertificateVerify(certificate_verify) => {
                HandshakeType::CertificateVerify.encode_to(to);
                certificate_verify.encode_to(&mut message_vec);
            },
            Handshake::Finished(finished) => {
                HandshakeType::Finished.encode_to(to);
                finished.encode_to(&mut message_vec);
            },
            Handshake::NewSessionTicket(new_session_ticket) => {
                HandshakeType::NewSessionTicket.encode_to(to);
                new_session_ticket.encode_to(&mut message_vec);
            },
            Handshake::Unsupported(handshake_type, data) => {
                handshake_type.encode_to(to);
                message_vec.extend_from_slice(&data);
            }
        }

        to.extend_from_slice(&transmute::u32_to_bytes_be(message_vec.len() as u32)[1..]);
        to.extend_from_slice(&message_vec);
    }
}

impl<'a> DecodeFrom<'a> for Handshake {
    fn decode(bytes:&[u8]) -> Result<(Handshake, usize, &[u8]), TLSError> {
        let (handshake_type, msg_type_size, bytes) = HandshakeType::decode(bytes)?;
        let remain_bytes_size = 3;
        let remain_bytes:usize = transmute::u32_from_bytes_be(&bytes[..remain_bytes_size]) as usize;
        let bytes = &bytes[remain_bytes_size ..];

        let (message, message_size, bytes) = match handshake_type {
            HandshakeType::ClientHello => {
                let (client_hello, size, bytes) = ClientHello::decode(bytes)?;
                (Handshake::ClientHello(client_hello), size, bytes)
            },
            HandshakeType::ServerHello => {
                let (server_hello, size, bytes) = ServerHello::decode(bytes)?;
                (Handshake::ServerHello(server_hello), size, bytes)
            },
            HandshakeType::EncryptedExtensions => {
                let (encrypted_extensions, size, bytes) = EncryptedExtensions::decode(bytes)?;
                (Handshake::EncryptedExtensions(encrypted_extensions), size, bytes)
            },
            HandshakeType::Certificate => {
                let (certificate, size, bytes) = Certificate::decode(bytes)?;
                (Handshake::Certificate(certificate), size, bytes)
            },
            HandshakeType::CertificateVerify => {
                let (certificate_verify, size, bytes) = CertificateVerify::decode(bytes)?;
                (Handshake::CertificateVerify(certificate_verify), size, bytes)
            },
            HandshakeType::Finished => {
                let (finished, size, bytes) = Finished::decode(bytes)?;
                (Handshake::Finished(finished), size, bytes)
            },
            HandshakeType::NewSessionTicket => {
                let (new_session_ticket, size, bytes) = NewSessionTicket::decode(bytes)?;
                (Handshake::NewSessionTicket(new_session_ticket), size, bytes)
            },
            handshake_type => {
                let data = Box::from(&bytes[..remain_bytes]);
                (Handshake::Unsupported(handshake_type, data), remain_bytes, &bytes[remain_bytes ..])
            }
        };

        Ok((message, msg_type_size + remain_bytes_size + message_size, bytes))
    }
}

impl Handshake {
    pub fn to_handshake_type(&self) -> HandshakeType {
        match self {
            Handshake::ServerHello(_) => {
                HandshakeType::ServerHello
            },
            Handshake::EncryptedExtensions(_) => {
                HandshakeType::EncryptedExtensions
            },
            Handshake::ClientHello(_) => {
                HandshakeType::ClientHello
            },
            Handshake::Certificate(_) => {
                HandshakeType::Certificate
            },
            Handshake::CertificateVerify(_) => {
                HandshakeType::CertificateVerify
            },
            Handshake::Finished(_) => {
                HandshakeType::Finished
            },
            Handshake::NewSessionTicket(_) => {
                HandshakeType::NewSessionTicket
            },
            Handshake::Unsupported(handshake_type, _) => {
                *handshake_type
            }
        }
    }
}