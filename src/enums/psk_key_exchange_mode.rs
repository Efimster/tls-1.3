use crate::enums::tls_error::TLSError;
use crate::enums::alert_description::AlertDescription;
use crate::utils::transmute::EncodeTo;
use crate::utils::transmute::DecodeFrom;

const KE:u8 = 0;
const DHE_KE: u8 = 1;

#[derive(Debug)]
pub enum PskKeyExchangeMode {
    PskKe,
    PskDheKe,
}

impl<'a> DecodeFrom<'a> for PskKeyExchangeMode {
    fn decode(bytes:&[u8]) -> Result<(PskKeyExchangeMode, usize, &[u8]), TLSError> {
        let mode = match bytes[0] {
            KE => PskKeyExchangeMode::PskKe,
            DHE_KE => PskKeyExchangeMode::PskDheKe,
            _ => return Err(TLSError::Alert(AlertDescription::DecodeError)),
        };
        Ok((mode, 1, &bytes[1..]))
    }
}

impl EncodeTo for PskKeyExchangeMode {
    fn encode_to(&self, to: &mut Vec<u8>){
        let value = match self {
            PskKeyExchangeMode::PskKe => KE,
            PskKeyExchangeMode::PskDheKe => DHE_KE,
        };
        to.push(value);
    }
}