use std::mem;
use std::cmp;
use crate::enums::tls_error::TLSError;

pub trait EncodeTo {
    fn encode_to(&self, to: &mut Vec<u8>);
    fn encode(&self) -> Box<[u8]> {
        let mut vec = vec![];
        self.encode_to(&mut vec);
        vec.into_boxed_slice()
    }
}

pub trait DecodeFrom<'a> {
    fn decode(bytes:&'a [u8]) -> Result<(Self, usize, &'a [u8]), TLSError> where Self: std::marker::Sized;
}

pub fn u16_from_bytes_be(bytes:&[u8]) -> u16{
    let length = cmp::min(bytes.len(), 2);
    u64_from_bytes_be(&bytes[..length]) as u16
}

pub fn u32_from_bytes_be(bytes: &[u8]) -> u32 {
    let length = cmp::min(bytes.len(), 4);
    u64_from_bytes_be(&bytes[..length]) as u32
}

pub fn u64_from_bytes_be(bytes: &[u8]) -> u64 {
    let mut result: [u8; 8] = [0; 8];

    if bytes.len() < 8 {
        let start = 8 - bytes.len();
        &result[start..].copy_from_slice(bytes);
    } else {
        result.copy_from_slice(bytes);
    }

//    result.reverse();

    let result:u64 = unsafe { mem::transmute(result) };
    result.to_be()
}

pub fn create_buffer(length:usize) -> Box<[u8]>{
    let mut buff: Vec<u8> = Vec::with_capacity(length);
    unsafe { buff.set_len(length); }
    let result:Box<[u8]> = buff.into_boxed_slice();
    result
}

pub fn decode_slice(bytes:&[u8], length_bytes_count:usize) -> (&[u8], usize, &[u8]) {
    let length = u64_from_bytes_be(&bytes[..length_bytes_count]) as usize;
    let total_bytes = length_bytes_count + length;
    //let mut buff= create_buffer(length);
    //buff.copy_from_slice(&bytes[length_bytes_count .. total_bytes]);
    (&bytes[length_bytes_count..total_bytes], total_bytes, &bytes[total_bytes..])
}

#[inline]
pub fn u16_to_bytes_be(value:u16) -> [u8; 2] {
    unsafe {mem::transmute::<u16, [u8;2]>(value.to_be())}
}

#[inline]
pub fn u32_to_bytes_be(value: u32) -> [u8; 4] {
    unsafe { mem::transmute::<u32, [u8; 4]>(value.to_be()) }
}

#[inline]
pub fn u64_to_bytes_be(value: u64) -> [u8; 8] {
    unsafe { mem::transmute::<u64, [u8; 8]>(value.to_be()) }
}

pub fn encode_slice_to(to:&mut Vec<u8>, bytes:&[u8], length_bytes_count: usize) {
    let len = bytes.len();
    match length_bytes_count {
        1 => to.push(len as u8),
        2 => to.extend_from_slice(&u16_to_bytes_be(len as u16)),
        3 => to.extend_from_slice(&u32_to_bytes_be(len as u32)[1..]),
        4 => to.extend_from_slice(&u32_to_bytes_be(len as u32)),
        8 => to.extend_from_slice(&u64_to_bytes_be(len as u64)),
        _ => (),
    };

    to.extend_from_slice(bytes);
}

pub fn encode_list_to<T: EncodeTo>(to: &mut Vec<u8>, slice: &[T], length_bytes_count: usize) {
    let mut vec:Vec<u8> = Vec::new();
    for item in slice.iter() {
        item.encode_to(&mut vec);
    }
    encode_slice_to(to, &vec, length_bytes_count)
}

pub fn prepend_encode_length_to(to: &mut Vec<u8>, length_bytes_count: usize) {
    let len = to.len();
    let mut result_vec:Vec<u8> = Vec::with_capacity(to.len() + 16);

    match length_bytes_count {
        1 => result_vec.push(len as u8),
        2 => result_vec.extend_from_slice(&u16_to_bytes_be(len as u16)),
        3 => result_vec.extend_from_slice(&u32_to_bytes_be(len as u32)[..3]),
        4 => result_vec.extend_from_slice(&u32_to_bytes_be(len as u32)),
        8 => result_vec.extend_from_slice(&u64_to_bytes_be(len as u64)),
        _ => (),
    };

    result_vec.append(to);
    mem::swap(to, &mut result_vec);
}

#[inline]
pub fn encode_u16_to(to: &mut Vec<u8>, value: u16){
    to.extend_from_slice(&u16_to_bytes_be(value));
}

#[inline]
pub fn encode_u32_to(to: &mut Vec<u8>, value: u32) {
    to.extend_from_slice(&u32_to_bytes_be(value));
}

#[inline]
pub fn encode_u64_to(to: &mut Vec<u8>, value: u64) {
    to.extend_from_slice(&u64_to_bytes_be(value));
}

pub fn concat_u8_slices(lhs:&[u8], rhs:&[u8]) -> Box<[u8]>{
    let mut result:Vec<u8> = Vec::with_capacity(lhs.len() + rhs.len());
    result.extend_from_slice(lhs);
    result.extend_from_slice(rhs);
    result.into_boxed_slice()
}

pub fn decode_list<'a, T:DecodeFrom<'a>>(bytes: &'a [u8], length_bytes_count: usize) -> Result<(Box<[T]>, usize, &'a[u8]), TLSError> {
    let (mut inner_bytes, total_size, bytes) = decode_slice(bytes, length_bytes_count);
    let mut result: Vec<T> = Vec::new();
    while inner_bytes.len() > 0 {
        let (value, size, _) = T::decode(inner_bytes)?;
        result.push(value);
        inner_bytes = &inner_bytes[size..];
    }

    Ok((result.into_boxed_slice(), total_size, bytes))
}

pub fn decode_list_with<'a, F, T>(bytes: &'a [u8], length_bytes_count: usize, func:F) -> Result<(Box<[T]>, usize, &'a [u8]), TLSError>
    where F : Fn( & 'a[u8]) -> Result<(T, usize, &'a [u8]), TLSError>
{
    let (mut inner_bytes, total_size, bytes) = decode_slice(bytes, length_bytes_count);

    let mut result: Vec<T> = Vec::new();
    while inner_bytes.len() > 0 {
        let (value, size, _) = func(inner_bytes)?;
        result.push(value);
        inner_bytes = &inner_bytes[size..];
    }

    Ok((result.into_boxed_slice(), total_size, bytes))
}