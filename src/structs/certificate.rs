use crate::enums::extension::Extension;
use crate::utils::transmute::EncodeTo;
use crate::utils::transmute;
use crate::utils::transmute::DecodeFrom;
use crate::enums::tls_error::TLSError;

#[derive(Debug)]
pub struct Certificate {
    pub certificate_request_context:Box<[u8]>, //< 0..2 ^ 8 - 1 >;
    pub certificate_list:Box<[CertificateEntry]>, // < 0..2 ^ 24 - 1 >;
}

#[derive(Debug)]
pub struct CertificateEntry {
    pub cert_data:Box<[u8]>, //< 1..2 ^ 24 - 1 >;
    pub extensions: Box<[Extension]>,  //< 6..2 ^ 16 - 1 >;
}

impl EncodeTo for Certificate {
    fn encode_to(&self, to: &mut Vec<u8>) {
        transmute::encode_slice_to(to, &self.certificate_request_context, 1);
        transmute::encode_list_to(to, &self.certificate_list, 3);
    }
}

impl<'a> DecodeFrom<'a> for Certificate {
    fn decode(bytes: &[u8]) -> Result<(Certificate, usize, &[u8]), TLSError> {

        let (certificate_request_context, context_size, bytes) = transmute::decode_slice(bytes, 1);
        let (certificate_list, list_size, bytes) = transmute::decode_list(bytes, 3)?;
        Ok((Certificate {
            certificate_request_context: Box::from(certificate_request_context),
            certificate_list,
        }, context_size + list_size, bytes))
    }
}

impl EncodeTo for CertificateEntry {
    fn encode_to(&self, to: &mut Vec<u8>) {
        transmute::encode_slice_to(to, &self.cert_data, 3);
        transmute::encode_list_to(to, &self.extensions, 2);
    }
}

impl<'a> DecodeFrom<'a> for CertificateEntry {
    fn decode(bytes: &[u8]) -> Result<(CertificateEntry, usize, &[u8]), TLSError> {
        let (cert_data, cert_data_size, bytes) = transmute::decode_slice(bytes, 3);
        let (extensions, extensions_size, bytes) =
            transmute::decode_list(bytes, 2)?;

        Ok((CertificateEntry {
            cert_data: Box::from(cert_data),
            extensions,
        }, cert_data_size + extensions_size, bytes))

    }
}