use crate::enums::extension::Extension;
use crate::utils::transmute;
use crate::utils::transmute::EncodeTo;
use crate::enums::tls_error::TLSError;
use crate::utils::transmute::DecodeFrom;

#[derive(Debug)]
pub struct EncryptedExtensions {
    pub extensions: Box<[Extension]>,  //< 0..2 ^ 16 - 1 >;
}

impl EncodeTo for EncryptedExtensions {
    fn encode_to(&self, to: &mut Vec<u8>) {
        transmute::encode_list_to(to, &self.extensions, 2);
    }
}

impl<'a> DecodeFrom<'a> for EncryptedExtensions {
    fn decode(bytes: &[u8]) -> Result<(EncryptedExtensions, usize, &[u8]), TLSError> {
        let (extensions, extensions_size, bytes) =
            transmute::decode_list_with(bytes, 2, Extension::decode_server_extension)?;

        Ok((EncryptedExtensions {
            extensions
        }, extensions_size, bytes))
    }
}