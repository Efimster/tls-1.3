use crate::enums::cipher_suite::CipherSuite;
use crate::enums::tls_error::TLSError;
use crate::structs::tls_cipher_text::TLSCipherText;
use crate::enums::tls_record_content_type::TLSRecordContentType;
use std::cmp;
use std::io::Write;
use std::io;
use std::net::{TcpStream, Shutdown};
use crate::structs::tls_plain_text::TLSPlainText;

const MAX_RECORD_SIZE:usize = 1500;

pub struct HandshakeWriter<W:Write> {
    cipher_suite: Option<CipherSuite>,
    sender_key: Box<[u8]>,
    sender_iv: Box<[u8]>,
    sequence_number: usize,
    pub stream_writer: W,
}

impl<W:Write> HandshakeWriter<W> {
    pub fn new(stream_writer:W) -> HandshakeWriter<W> {
        HandshakeWriter {
            cipher_suite: None,
            sender_key: Box::new([]),
            sender_iv: Box::new([]),
            sequence_number: 0,
            stream_writer,
        }
    }

    pub fn write(&mut self, data: &[u8]) -> Result<(), TLSError> {
        let mut bytes = data;

        match self.cipher_suite {
            None => {
                while bytes.len() > 0 {
                    let len = cmp::min(bytes.len(), MAX_RECORD_SIZE);
                    TLSPlainText::write(&mut self.stream_writer, &bytes[..len], TLSRecordContentType::Handshake)?;
                    bytes = &bytes[len ..];
                }
            },
            Some(cipher) => {
                while bytes.len() > 0 {
                    let len = cmp::min(bytes.len(), MAX_RECORD_SIZE);
                    TLSCipherText::write(&mut self.stream_writer, &bytes[..len], TLSRecordContentType::Handshake, cipher,
                        self.sequence_number as u64, &self.sender_key, &self.sender_iv)?;
                    bytes = &bytes[len..];
                    self.sequence_number += 1;
                }
            }
        }

        return Ok(());
    }


    pub fn set_cipher_suit(&mut self, cipher_suite: CipherSuite) {
        self.cipher_suite = Some(cipher_suite);
    }

    pub fn set_sender_key(&mut self, sender_write_key: &[u8], sender_write_iv: &[u8]) {
        self.sender_key = Box::from(sender_write_key);
        self.sender_iv = Box::from(sender_write_iv);
        self.sequence_number = 0;
    }
}

pub struct TLSWriter<W: Write> {
    cipher_suite: CipherSuite,
    sender_key: Box<[u8]>,
    sender_iv: Box<[u8]>,
    sequence_number: usize,
    stream_writer: W,
}

impl<W: Write> TLSWriter<W> {
    pub fn new(stream_writer: W, cipher_suite: CipherSuite, sender_key: Box<[u8]>, sender_iv: Box<[u8]>) -> TLSWriter<W> {
        TLSWriter {
            cipher_suite,
            sender_key,
            sender_iv,
            sequence_number: 0,
            stream_writer,
        }
    }
}

impl<W:Write> Write for TLSWriter<W> {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        TLSCipherText::write(&mut self.stream_writer, &buf, TLSRecordContentType::ApplicationData, self.cipher_suite,
    self.sequence_number as u64, &self.sender_key, &self.sender_iv)?;
        self.sequence_number += 1;
        Ok(buf.len())
    }

    fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }
}

impl TLSWriter<TcpStream> {
    pub fn shutdown(&self) -> io::Result<()> {
        self.stream_writer.shutdown(Shutdown::Write)
    }
}