use crate::enums::tls_record_content_type::TLSRecordContentType;
use crate::enums::alert_description::AlertDescription;
use crate::enums::tls_error::TLSError;
use crate::utils::transmute::DecodeFrom;
use crate::structs::tls_cipher_text::TLSCipherText;
use crate::utils::transmute;
use crate::enums::tls_record_content_type;
use crate::enums::cipher_suite::CipherSuite;
use crate::utils::transmute::EncodeTo;
use crate::structs::tls_plain_text::TLSPlainText;
use sha::cha_cha_poly::cha_cha_20_aead_encrypt;

pub struct TLSInnerPlainText {
    pub content: Box<[u8]>,
    pub content_type: TLSRecordContentType,
    pub zeros: Box<[u8]>
}

impl TLSInnerPlainText {
    pub fn from_bytes(bytes:&[u8]) -> Result<TLSInnerPlainText, TLSError> {
        let mut index = bytes.len() - 1;
        while index > 0 && bytes[index] == 0 {
            index -= 1;
        }

        let (content_type,_ ,_) = TLSRecordContentType::decode(&bytes[index..])?;

        if content_type != TLSRecordContentType::ApplicationData && index == 0 {
            return Err(TLSError::Alert(AlertDescription::UnexpectedMessage));
        }

        Ok(TLSInnerPlainText {
            content: bytes[..index].to_vec().into_boxed_slice(),
            content_type,
            zeros: bytes[index + 1..].to_vec().into_boxed_slice(),
        })
    }

    pub fn to_tls_cipher_text(self, cipher_suite: CipherSuite, write_sequence_number: u64,
                              sender_write_key: &[u8], sender_write_iv: &[u8]) -> Result<TLSCipherText, TLSError>
    {
        let total_len = self.encode_len();
        let encode_len_bytes = transmute::u16_to_bytes_be(total_len as u16);
        let additional_data: [u8; 5] = [tls_record_content_type::APPLICATION_DATA, 0x03, 0x03,
            encode_len_bytes[0], encode_len_bytes[1]];

        let mut text: Vec<u8> = Vec::with_capacity(total_len);
        self.encode_to(&mut text);

        match cipher_suite {
            CipherSuite::ChaCha20Poly1305Sha256 => {
                let mut write_key: [u8; 32] = [0; 32];
                write_key.copy_from_slice(&sender_write_key);

                let mut nonce: [u8; 12] = [0; 12];
                &nonce[4..].copy_from_slice(&transmute::u64_to_bytes_be(write_sequence_number));
                for i in 0..nonce.len() {
                    nonce[i] ^= sender_write_iv[i];
                }

                let (encrypted, tag) = cha_cha_20_aead_encrypt(&additional_data, &write_key, nonce, &text);
                let mut encrypted_record: Vec<u8> = Vec::with_capacity(encrypted.len() + tag.len());
                encrypted_record.extend_from_slice(&encrypted);
                encrypted_record.extend_from_slice(&tag);
                let encrypted_record = encrypted_record.into_boxed_slice();

                Ok(TLSCipherText {
                    encrypted_record
                })
            },
            _ => Err(TLSError::Alert(AlertDescription::InternalError)),
        }
    }

    pub fn to_plain_text(self) -> TLSPlainText {
        TLSPlainText {
            content_type: self.content_type,
            fragment: self.content,
        }
    }

    pub fn encode_to(&self, to: &mut Vec<u8>) {
        to.extend_from_slice(&self.content);
        self.content_type.encode_to(to);
        to.extend_from_slice(&self.zeros);
    }

    #[inline]
    fn encode_len(&self) -> usize {
        self.content.len() + self.zeros.len() + 1 + 16
    }
}