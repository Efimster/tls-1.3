use crate::utils::transmute::EncodeTo;
use crate::utils::transmute::DecodeFrom;
use crate::enums::tls_error::TLSError;
use crate::enums::alert_description::AlertDescription;

const OCSP_TYPE: u8 = 1;

#[derive(Debug)]
pub enum CertificateStatusType {
    OCSP,
}

impl EncodeTo for CertificateStatusType {
    fn encode_to(&self, to: &mut Vec<u8>) {
        to.push(OCSP_TYPE);
    }
}

impl<'a> DecodeFrom<'a> for CertificateStatusType {
    fn decode(bytes: &'a [u8]) -> Result<(Self, usize, &'a [u8]), TLSError> {
        let result = match bytes[0] {
            1 => CertificateStatusType::OCSP,
            _ => return Err(TLSError::Alert(AlertDescription::DecodeError)),
        };
        Ok((result, 1, &bytes[1..]))
    }
}