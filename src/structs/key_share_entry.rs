use crate::utils::transmute;
use crate::enums::named_group::NamedGroup;
use crate::utils::transmute::EncodeTo;
use crate::utils::transmute::DecodeFrom;
use crate::enums::tls_error::TLSError;

#[derive(Debug)]
pub struct KeyShareEntry {
    pub group: NamedGroup,
    pub key_exchange: Box<[u8]>, //< 1..2 ^ 16 -1 >
}


impl<'a> DecodeFrom<'a> for KeyShareEntry {
    fn decode(bytes: &[u8]) -> Result<(KeyShareEntry, usize, &[u8]), TLSError>  {
        let (name_group, name_group_size, bytes) = NamedGroup::decode(&bytes)?;
        let (key_exchange, key_exchange_size, bytes) = transmute::decode_slice(bytes, 2);
        Ok((KeyShareEntry{
            group: name_group,
            key_exchange: Box::from(key_exchange),
        }, name_group_size + key_exchange_size, bytes))
    }
}

impl EncodeTo for KeyShareEntry {
    fn encode_to(&self, to: &mut Vec<u8>) {
        self.group.encode_to(to);
        transmute::encode_slice_to(to, &self.key_exchange, 2);
    }
}