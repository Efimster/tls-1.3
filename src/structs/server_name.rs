use crate::enums::name_type::NameType;
use crate::utils::transmute;
use crate::enums::tls_error::TLSError;
use crate::utils::transmute::DecodeFrom;
use std::str;
use crate::utils::transmute::EncodeTo;

#[derive(Debug)]
pub struct ServerName {
    pub name_type: NameType,
    pub host_name: String,
}

impl EncodeTo for ServerName {
    fn encode_to(&self, to: &mut Vec<u8>) {
        self.name_type.encode_to(to);
        transmute::encode_slice_to(to, &self.host_name.as_bytes(), 2);
    }
}

impl<'a> DecodeFrom<'a> for ServerName {
    fn decode(bytes:&[u8]) -> Result<(ServerName, usize, &[u8]), TLSError>{
        let (name_type, type_size, bytes) = NameType::decode(bytes)?;
        let (host_name, name_size, bytes) = transmute::decode_slice(bytes, 2);
        let host_name = str::from_utf8(host_name)?;
        Ok((ServerName {
            name_type,
            host_name: host_name.to_string(),
        }, type_size + name_size, bytes))
    }
}