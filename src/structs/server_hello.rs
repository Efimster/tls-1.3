use crate::enums::protocol_version::ProtocolVersion;
use crate::enums::cipher_suite::CipherSuite;
use crate::enums::extension::Extension;
use crate::utils::transmute;
use crate::utils::transmute::EncodeTo;
use crate::utils::transmute::DecodeFrom;
use crate::enums::tls_error::TLSError;
use crate::enums::named_group::NamedGroup;
use crate::enums::alert_description::AlertDescription;

#[derive(Debug)]
pub struct ServerHello{
    pub legacy_version: ProtocolVersion, // = 0x0303, /* TLS v1.2 */
    pub random: [u8; 32],
    pub legacy_session_id_echo: Box<[u8]>,
    pub cipher_suit: CipherSuite,
    pub legacy_compression_method: u8, // = 0;
    pub extensions: Box<[Extension]>,  //< 6..2 ^ 16 - 1 >;
}

impl ServerHello {
    pub fn check_validity(&self) -> Result<(), TLSError> {
        for extension in self.extensions.iter() {
            match extension {
                Extension::SupportedVersionsServerHello { selected_version } => {
                    if *selected_version != ProtocolVersion::TLS13 {
                        return Err(TLSError::Alert(AlertDescription::ProtocolVersion));
                    }
                },
                _ => (),
            }
        }

        Ok(())
    }

    pub fn get_server_share(&self, group: NamedGroup) -> Result<&[u8], TLSError> {
        for extension in self.extensions.iter() {
            match extension {
                Extension::KeyShareServerHello(server_share) if server_share.group == group => {
                    return Ok(&server_share.key_exchange);
                },
                _ => (),
            }
        }

        Err(TLSError::Alert(AlertDescription::InsufficientSecurity))
    }
}

impl EncodeTo for ServerHello {
    fn encode_to(&self, to: &mut Vec<u8>) {
        self.legacy_version.encode_to(to);
        to.extend_from_slice(&self.random);
        transmute::encode_slice_to(to, &self.legacy_session_id_echo, 1);
        self.cipher_suit.encode_to(to);
        to.push(0u8);
        transmute::encode_list_to(to, &self.extensions, 2);
    }
}

impl<'a> DecodeFrom<'a> for ServerHello {
    fn decode(bytes: &[u8]) -> Result<(ServerHello, usize, &[u8]), TLSError> {
        let (legacy_version, legacy_version_size, mut bytes) = ProtocolVersion::decode(bytes)?;
        let mut random: [u8; 32] = [0; 32];
        random.copy_from_slice(&bytes[..32]);
        bytes = &bytes[random.len()..];
        let (legacy_session_id_echo, legacy_session_id_echo_size, bytes) = transmute::decode_slice(bytes, 1);
        let (cipher_suit, cipher_suit_size, mut bytes) = CipherSuite::decode(bytes)?;
        bytes = &bytes[1..];

        let (extensions, extensions_size, bytes) =
            transmute::decode_list_with(bytes, 2, Extension::decode_server_extension)?;

        let total_size = legacy_version_size + random.len() + legacy_session_id_echo_size
            + cipher_suit_size + 1 + extensions_size;

        Ok((ServerHello {
            legacy_version,
            random,
            legacy_session_id_echo: Box::from(legacy_session_id_echo),
            cipher_suit,
            legacy_compression_method: 0u8,
            extensions,
        }, total_size, bytes))
    }
}