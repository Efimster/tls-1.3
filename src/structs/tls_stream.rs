use std::net::{TcpStream, ToSocketAddrs, TcpListener, SocketAddr};
use crate::structs::tls_reader::TLSReader;
use crate::structs::tls_writer::TLSWriter;
use crate::structs::client_handshake::ClientHandshake;
use std::io::{Read, Write};
use std::io;
use crate::enums::tls_error::TLSError;
use x509::private_key::PrivateKey;
use crate::enums::signature_scheme::SignatureScheme;
use crate::structs::server_handshake::ServerHandshake;
use std::time::Duration;

pub struct TLSStream<R: Read, W: Write> {
    pub reader: TLSReader<R>,
    pub writer: TLSWriter<W>,
}

impl TLSStream<TcpStream, TcpStream> {
    pub fn connect<A: ToSocketAddrs>(addr: A) -> Result<TLSStream<TcpStream, TcpStream>, TLSError> {
        match TcpStream::connect(addr) {
            Ok(stream) => {
                let reader = stream.try_clone().unwrap();
                let mut handshake = ClientHandshake::new(reader, stream);
                let (reader, writer) = handshake.connect()?;
                Ok(TLSStream {
                    reader,
                    writer,
                })
            },
            Err(e) => Err(e.into()),
        }
    }

    pub fn incoming<'a>(listener:&'a TcpListener, certificates:&'a [&'a [u8]], private_key:&'a PrivateKey) -> Wrapper<'a> {
        Wrapper{
            listener,
            certificates,
            private_key,
        }
    }

    pub fn peer_addr(&self) -> io::Result<SocketAddr> {
        self.reader.inner().peer_addr()
    }

    pub fn set_read_timeout(&self, dur: Option<Duration>) -> io::Result<()> {
        self.reader.set_read_timeout(dur)
    }
}

impl<R: Read, W: Write> Read for TLSStream<R, W> {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize>{
        self.reader.read(buf)
    }
}

impl<R: Read, W: Write> Write for TLSStream<R, W> {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize>{
        self.writer.write(buf)
    }
    fn flush(&mut self) -> io::Result<()>{
        self.writer.flush()
    }
}

pub struct Wrapper<'a> {
    listener: &'a TcpListener,
    certificates: &'a[&'a [u8]],
    private_key: &'a PrivateKey,
}

impl<'a> Iterator for Wrapper<'a> {
    type Item = Result<TLSStream<TcpStream, TcpStream>, TLSError>;

    fn next(&mut self) -> Option<Self::Item> {
        let result = match self.listener.accept() {
            Ok((stream, _)) => {
                let reader = stream.try_clone().unwrap();
                let mut session = ServerHandshake::new(reader, stream, self.certificates,
                   self.private_key, SignatureScheme::EcdsaSecp256r1Sha256);
                match session.listen() {
                    Ok((reader, writer)) => Ok(TLSStream {
                        reader,
                        writer,
                    }),
                    Err(err) => Err(err),
                }
            },
            Err(e) => Err(e.into()),
        };
        Some(result)
    }
}

