use crate::utils::transmute;
use crate::enums::tls_error::TLSError;
use crate::enums::alert_description::AlertDescription;
use crate::enums::cipher_suite::CipherSuite;
use crate::enums::extension::Extension;
use crate::enums::protocol_version::ProtocolVersion;
use crate::enums::named_group::NamedGroup;
use crate::utils::transmute::EncodeTo;
use crate::utils::transmute::DecodeFrom;

type Random = [u8; 32];

#[derive(Debug)]
pub struct ClientHello {
    pub legacy_version: ProtocolVersion,  // = 0x0303; /* TLS v1.2 */
    pub random: Random,
    pub legacy_session_id: Box<[u8]>, // < 0..32 >;
    pub cipher_suites: Box<[CipherSuite]>, //< 2..2^ 16 - 2 >;
    pub legacy_compression_methods: Box<[u8]>, //  < 1..2 ^ 8 - 1 >;
    pub extensions: Box<[Extension]>, //< 8..2 ^ 16 - 1 >;,
}

impl ClientHello {
    pub fn check_validity(&self) -> Result < (), TLSError > {
        for extension in self.extensions.iter() {
            match extension {
                Extension::SupportedVersionsClientHello(versions) => {
                    if !versions.iter().any(|version| *version == ProtocolVersion::TLS13) {
                        return Err(TLSError::Alert(AlertDescription::ProtocolVersion));
                    }
                },
                Extension::SupportedGroups(named_group_list) => {
                    if !named_group_list.iter().any(|group| *group == NamedGroup::X25519) {
                        return Err(TLSError::Alert(AlertDescription::InsufficientSecurity));
                    }
                },
                _ => (),
            }
        }

        Ok(())
    }
    pub fn get_client_share(&self, group:NamedGroup) -> Option<&[u8]> {
        for extension in self.extensions.iter() {
            match extension {
                Extension::KeyShareClientHello {client_shares} => {
                    for share in client_shares.iter() {
                        if share.group == group {
                            return Some(&share.key_exchange);
                        }
                    }
                    break;
                },
                _ => (),
            }
        }

        None
    }
}

impl EncodeTo for ClientHello {
    fn encode_to(&self, to: &mut Vec<u8>) {
        self.legacy_version.encode_to(to);
        to.extend_from_slice(&self.random);
        transmute::encode_slice_to(to, &self.legacy_session_id, 1);
        transmute::encode_list_to(to, &self.cipher_suites, 2);
        transmute::encode_slice_to(to, &self.legacy_compression_methods, 1);
        transmute::encode_list_to(to, &self.extensions, 2);
    }
}

impl<'a> DecodeFrom<'a> for ClientHello {
    fn decode(bytes: &[u8]) -> Result<(ClientHello, usize, &[u8]), TLSError> {
        let (legacy_version, legacy_version_size, bytes) = ProtocolVersion::decode(bytes)?;
        let mut random: Random = [0; 32];
        random.copy_from_slice(&bytes[..32]);
        let bytes = &bytes[random.len()..];

        let (legacy_session_id, legacy_session_id_size, bytes) = transmute::decode_slice(bytes, 1);
        let (cipher_suites, cipher_suites_size, bytes) = transmute::decode_list(bytes, 2)?;
        let (legacy_compression_methods, legacy_compression_methods_size, bytes) = transmute::decode_slice(&bytes, 1);
        if legacy_compression_methods.len() != 1 || legacy_compression_methods[0] != 0 {
            return Err(TLSError::Alert(AlertDescription::IllegalParameter));
        }
        let (extensions, extensions_size, bytes) =
            transmute::decode_list_with(bytes, 2, Extension::decode_client_extension)?;
        let cipher_suites = cipher_suites.iter()
            .filter(|&cipher| if let CipherSuite::Unknown(_) = cipher { false } else { true })
            .cloned()
            .collect::<Vec<CipherSuite>>();
        let total_size = legacy_version_size + random.len() + legacy_session_id_size + cipher_suites_size
            + legacy_compression_methods_size + extensions_size;
        Ok((ClientHello {
            legacy_version,
            random,
            legacy_session_id: Box::from(legacy_session_id),
            cipher_suites: cipher_suites.into_boxed_slice(),
            legacy_compression_methods: Box::from(legacy_compression_methods),
            extensions,
        }, total_size, bytes))
    }
}