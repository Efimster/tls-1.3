use crate::enums::tls_record_content_type::TLSRecordContentType;
use crate::enums::tls_error::TLSError;
use std::io::{Read, Write};
use crate::utils::transmute::{DecodeFrom, EncodeTo};
use crate::utils::transmute;
use crate::enums::protocol_version::ProtocolVersion;
use crate::structs::tls_cipher_text::TLSCipherText;

#[derive(Debug)]
pub struct TLSPlainText {
    pub content_type: TLSRecordContentType,
    pub fragment: Box<[u8]>,
}

impl TLSPlainText {
    pub fn read<R: Read>(stream_reader: &mut R) -> Result<TLSPlainText, TLSError> {
        loop {
            let record = Self::read_record(stream_reader)?;
            if record.content_type != TLSRecordContentType::ChangeCipherSpec {
                return Ok(record);
            }
        }
    }

    pub fn read_record<R: Read>(stream_reader: &mut R) -> Result<TLSPlainText, TLSError> {
        let mut header = [0; 5];
        stream_reader.read_exact(&mut header)?;
        let (content_type, _, _) = TLSRecordContentType::decode(&header)?;
        let length: u16 = transmute::u16_from_bytes_be(&header[3..]);
        let mut buff = transmute::create_buffer(length as usize);
        stream_reader.read_exact(&mut buff)?;

        Ok(TLSPlainText {
            content_type,
            fragment: buff
        })
    }

    pub fn encode_to(&self, to: &mut Vec<u8>) {
        self.content_type.encode_to(to);
        ProtocolVersion::TLS12.encode_to(to);
        transmute::encode_u16_to(to, self.fragment.len() as u16);
        to.extend_from_slice(&self.fragment);
    }

    pub fn to_cipher_text(self) -> TLSCipherText {
        TLSCipherText {
            encrypted_record: self.fragment
        }
    }

    pub fn write<W: Write>(stream_writer: &mut W, data: &[u8], content_type: TLSRecordContentType) -> Result<(), TLSError> {
        let mut fragment: Vec<u8> = Vec::with_capacity(data.len());
        fragment.extend_from_slice(data);
        let tls_plain_text = TLSPlainText {
            content_type,
            fragment: fragment.into_boxed_slice(),
        };

        let mut bytes: Vec<u8> = Vec::with_capacity(tls_plain_text.encode_len());
        tls_plain_text.encode_to(&mut bytes);

        stream_writer.write(&bytes)?;
        Ok(())
    }

    #[inline]
    fn encode_len(&self) -> usize {
        self.fragment.len() + 5
    }
}