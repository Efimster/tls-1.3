use crate::enums::alert_description::AlertDescription;
use crate::enums::alert_level::AlertLevel;
use crate::enums::tls_error::TLSError;

#[derive(Debug)]
pub struct Alert {
    pub level: AlertLevel,
    pub description: AlertDescription,
}

impl Alert {
    pub fn decode(bytes:&[u8]) -> Result<Alert, TLSError> {
        Ok(Alert {
            level: AlertLevel::decode(bytes[0])?,
            description: AlertDescription::decode(bytes[1])?,
        })
    }
}