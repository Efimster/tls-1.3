use crate::enums::signature_scheme::SignatureScheme;
use crate::utils::transmute::DecodeFrom;
use crate::utils::transmute::EncodeTo;
use crate::utils::transmute;
use crate::enums::tls_error::TLSError;

#[derive(Debug)]
pub struct CertificateVerify {
    pub algorithm: SignatureScheme,
    pub signature: Box<[u8]>, // < 0..2 ^ 16 -1 >;
}

impl EncodeTo for CertificateVerify {
    fn encode_to(&self, to: &mut Vec<u8>) {
        self.algorithm.encode_to(to);
        transmute::encode_slice_to(to, &self.signature, 2);
    }
}

impl<'a> DecodeFrom<'a> for CertificateVerify {
    fn decode(bytes: &[u8]) -> Result<(CertificateVerify, usize, &[u8]), TLSError> {
        let (algorithm, schema_size, bytes) = SignatureScheme::decode(bytes)?;
        let (signature, signature_size, bytes) = transmute::decode_slice(bytes, 2);

        Ok((CertificateVerify {
            algorithm,
            signature: Box::from(signature),
        }, schema_size + signature_size, bytes))
    }
}