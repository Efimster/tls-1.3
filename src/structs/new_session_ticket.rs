use crate::enums::extension::Extension;
use crate::utils::transmute::{EncodeTo, DecodeFrom};
use crate::enums::tls_error::TLSError;
use crate::utils::transmute;

#[derive(Debug)]
pub struct NewSessionTicket {
    pub ticket_lifetime: u32,
    pub ticket_age_add: u32,
    pub ticket_nonce: Box<[u8]>, //<0..255>
    pub ticket: Box<[u8]>,  //<1..2^16-1>
    pub extensions: Box<[Extension]>,  //<0..2^16-2>
}

impl EncodeTo for NewSessionTicket {
    fn encode_to(&self, to: &mut Vec<u8>) {
        transmute::encode_u32_to(to, self.ticket_lifetime);
        transmute::encode_u32_to(to, self.ticket_lifetime);
        transmute::encode_slice_to(to, &self.ticket_nonce, 1);
        transmute::encode_list_to(to, &self.extensions, 2);
    }
}

impl<'a> DecodeFrom<'a> for NewSessionTicket {
    fn decode(bytes: &[u8]) -> Result<(NewSessionTicket, usize, &[u8]), TLSError> {
        let (ticket_lifetime, bytes) = (transmute::u32_from_bytes_be(bytes), &bytes[4..]);
        let (ticket_age_add, bytes) = (transmute::u32_from_bytes_be(bytes), &bytes[4..]);
        let (ticket_nonce, nonce_size, bytes) = transmute::decode_slice(bytes, 1);
        let (ticket, ticket_size, bytes) = transmute::decode_slice(bytes, 2);
        let (extensions, extensions_size, bytes) =
            transmute::decode_list_with(bytes, 2, Extension::decode_client_extension)?;
        let total_size = 8 + nonce_size + ticket_size + extensions_size;
        Ok((NewSessionTicket {
            ticket_lifetime,
            ticket_age_add,
            ticket_nonce: Box::from(ticket_nonce),
            ticket: Box::from(ticket),
            extensions,
        }, total_size, bytes))
    }
}