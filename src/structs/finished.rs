use crate::utils::transmute::DecodeFrom;
use crate::utils::transmute::EncodeTo;
use crate::enums::tls_error::TLSError;

#[derive(Debug)]
pub struct Finished {
    pub verify_data: Box<[u8]>,
}

impl EncodeTo for Finished {
    fn encode_to(&self, to: &mut Vec<u8>) {
        to.extend_from_slice(&self.verify_data);
    }
}

impl<'a> DecodeFrom<'a> for Finished {
    fn decode(bytes: &[u8]) -> Result<(Finished, usize, &[u8]), TLSError> {
        Ok((Finished {
            verify_data: Box::from(bytes),
        }, bytes.len(), &[]))
    }
}