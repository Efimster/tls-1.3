use crate::enums::cipher_suite::CipherSuite;
use std::io::Read;
use crate::enums::tls_error::TLSError;
use crate::structs::tls_cipher_text::TLSCipherText;
use crate::enums::tls_record_content_type::TLSRecordContentType;
use crate::enums::alert_description::AlertDescription;
use crate::enums::handshake::Handshake;
use crate::enums::handshake_type::HandshakeType;
use crate::utils::transmute;
use crate::utils::transmute::DecodeFrom;
use std::cmp;
use std::io::Write;
use std::io;
use crate::structs::alert::Alert;
use std::net::{TcpStream, Shutdown};
use std::time::Duration;
use crate::structs::tls_plain_text::TLSPlainText;

pub struct HandshakeReader<R: Read> {
    buffer: Option<TLSPlainText>,
    cipher_suite: Option<CipherSuite>,
    sender_write_key: Box<[u8]>,
    sender_write_iv: Box<[u8]>,
    sequence_number: usize,
    pub stream_reader: R,
}

impl<R: Read> HandshakeReader<R> {
    pub fn new(stream_reader:R) -> HandshakeReader<R> {
        HandshakeReader {
            buffer: None,
            cipher_suite: None,
            sender_write_key: Box::new([]),
            sender_write_iv: Box::new([]),
            sequence_number: 0,
            stream_reader,
        }
    }

    pub fn read(&mut self) -> Result<(Handshake, Box<[u8]>), TLSError>{
        const REMAIN_BYTES_SIZE:usize = 3;
        let mut plain_text  = match self.buffer.take() {
            Some(buffer) => buffer,
            None =>  match self.cipher_suite {
                None =>
                    TLSPlainText::read(&mut self.stream_reader)?,
                Some(cipher) => {
                    let plain_text = TLSCipherText::read(&mut self.stream_reader, cipher, self.sequence_number as u64,
                        &self.sender_write_key, &self.sender_write_iv)?;
                    self.sequence_number += 1;
                    plain_text
                },
            },
        };
        Self::check_plain_text(&plain_text)?;

        let (_, msg_type_size, bytes) = HandshakeType::decode(&plain_text.fragment)?;
        let mut remain_bytes: usize = transmute::u32_from_bytes_be(&bytes[..REMAIN_BYTES_SIZE]) as usize;

        let mut result_bytes = Vec::with_capacity(remain_bytes + REMAIN_BYTES_SIZE + msg_type_size);
        let mut partition_size = cmp::min(remain_bytes + REMAIN_BYTES_SIZE + msg_type_size, plain_text.fragment.len());
        result_bytes.extend_from_slice(&plain_text.fragment[..partition_size]);
        remain_bytes -= partition_size - REMAIN_BYTES_SIZE - msg_type_size;

        while remain_bytes > 0 {
            plain_text = match self.cipher_suite {
                None => TLSPlainText::read(&mut self.stream_reader)?,
                Some(cipher) => {
                    let plain_text = TLSCipherText::read(&mut self.stream_reader, cipher, self.sequence_number as u64,
                        &self.sender_write_key, &self.sender_write_iv)?;
                    self.sequence_number += 1;
                    Self::check_plain_text(&plain_text)?;
                    plain_text
                },
            };
            partition_size = cmp::min(remain_bytes, plain_text.fragment.len());
            result_bytes.extend_from_slice(&plain_text.fragment[..partition_size]);
            remain_bytes -= partition_size;
        }

        let (message, _, _) = Handshake::decode(&result_bytes)?;
        self.buffer = if plain_text.fragment[partition_size..].len() > 0 {
            Some(TLSPlainText {
                content_type: plain_text.content_type,
                fragment: Box::from(&plain_text.fragment[partition_size..]),
            })
        }
        else {
            None
        };


        return Ok((message, result_bytes.into_boxed_slice()));
    }

    pub fn set_cipher_suit(&mut self, cipher_suite: CipherSuite) {
        self.cipher_suite = Some(cipher_suite);
    }

    pub fn set_sender_key(&mut self, sender_write_key: &[u8], sender_write_iv: &[u8]) {
        self.sender_write_key = Box::from(sender_write_key);
        self.sender_write_iv = Box::from(sender_write_iv);
        self.sequence_number = 0;
    }

    pub fn check_plain_text(plain_text:&TLSPlainText) -> Result<(), TLSError> {
        match plain_text.content_type {
            TLSRecordContentType::Handshake => Ok(()),
            TLSRecordContentType::Alert => {
                let alert = Alert::decode(&plain_text.fragment)?;
                Err(TLSError::Alert(AlertDescription::Received(Box::new(alert.description))))
            },
            _ => {
                Err(TLSError::Alert(AlertDescription::UnexpectedMessage))
            }
        }
    }
}

pub struct TLSReader<R:Read> {
    read_buffer: Box<[u8]>,
    read_len: usize,
    cipher_suite: CipherSuite,
    sender_write_key: Box<[u8]>,
    sender_write_iv: Box<[u8]>,
    sequence_number: usize,
    stream_reader: R,
}

impl<R: Read> TLSReader<R> {
    pub fn new(stream_reader: R, cipher_suite: CipherSuite, sender_write_key: Box<[u8]>, sender_write_iv: Box<[u8]>) -> TLSReader<R> {
        TLSReader {
            read_buffer: Box::new([]),
            read_len: 0,
            cipher_suite,
            sender_write_key,
            sender_write_iv,
            sequence_number: 0,
            stream_reader,
        }
    }

    fn reset_read_buffer(&mut self) {
        self.read_buffer = Box::new([]);
        self.read_len = 0;
    }

    fn get_post_handshake_messages(&mut self, bytes: &[u8]) -> Result<Box<[Handshake]>, TLSError> {
        const REMAIN_BYTES_SIZE: usize = 3;
        let mut result = Vec::new();
        let mut plain_text;
        let mut bytes = bytes;
        while bytes.len() > 0 {
            let (_, msg_type_size, _) = HandshakeType::decode(&bytes)?;
            let mut remain_bytes: usize = transmute::u32_from_bytes_be(&bytes[msg_type_size..REMAIN_BYTES_SIZE+ msg_type_size]) as usize;
            let mut result_bytes = Vec::with_capacity(remain_bytes + REMAIN_BYTES_SIZE + msg_type_size);
            let mut partition_size = cmp::min(remain_bytes + REMAIN_BYTES_SIZE + msg_type_size, bytes.len());
            result_bytes.extend_from_slice(&bytes[..partition_size]);
            remain_bytes -= partition_size - REMAIN_BYTES_SIZE - msg_type_size;
            bytes = &bytes[partition_size..];

            while remain_bytes > 0 {
                plain_text = TLSCipherText::read(&mut self.stream_reader, self.cipher_suite,
                self.sequence_number as u64, &self.sender_write_key, &self.sender_write_iv)?;
                self.sequence_number += 1;
                HandshakeReader::<R>::check_plain_text(&plain_text)?;
                partition_size = cmp::min(remain_bytes, plain_text.fragment.len());
                result_bytes.extend_from_slice(&plain_text.fragment[..partition_size]);
                remain_bytes -= partition_size;
                bytes = &plain_text.fragment[partition_size..];
            }

            let (message, _, _) = Handshake::decode(&result_bytes)?;
            result.push(message);
        }

        Ok(result.into_boxed_slice())
    }

    pub fn inner(&self) -> &R {
        &self.stream_reader
    }
}

impl<R:Read> Read for TLSReader<R> {
    fn read(&mut self, mut buf: &mut [u8]) -> io::Result<usize> {
        assert_ne!(buf.len(), 0);
        let written = if self.read_len <  self.read_buffer.len() {
            let written= buf.write(&self.read_buffer[self.read_len..])?;
            self.read_len += written;
            written
        }
        else {
            let plain_text = TLSCipherText::read(&mut self.stream_reader, self.cipher_suite,
             self.sequence_number as u64, &self.sender_write_key, &self.sender_write_iv)?;
            self.sequence_number += 1;
            match plain_text.content_type {
                TLSRecordContentType::ApplicationData => {
                    self.read_buffer = plain_text.fragment;
                    let written = buf.write(&self.read_buffer)?;
                    self.read_len += written;
                    written
                },
                TLSRecordContentType::Handshake => {
                    let post_handshake_messages = self.get_post_handshake_messages(&plain_text.fragment)?;
                    for i in 0 .. post_handshake_messages.len() {
                        eprintln!("====== POST HANDSHAKE ({:?}) ============", post_handshake_messages[i].to_handshake_type());
                    }
                    self.read(buf)?
                },
                TLSRecordContentType::Alert => {
                    let alert = Alert::decode(&plain_text.fragment)?;
                    return Err(TLSError::Alert(alert.description).into());
                },
                TLSRecordContentType::ChangeCipherSpec => {
                    self.read(buf)?
                }
            }
        };

        if self.read_buffer.len() == self.read_len {
            self.reset_read_buffer();
        }

        Ok(written)
    }
}

impl TLSReader<TcpStream> {
    pub fn set_read_timeout(&self, dur: Option<Duration>) -> io::Result<()> {
        self.stream_reader.set_read_timeout(dur)
    }

    pub fn shutdown(&self) -> io::Result<()> {
        self.stream_reader.shutdown(Shutdown::Read)
    }
}