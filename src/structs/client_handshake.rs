use crate::enums::session_state::ClientSessionState;
use std::io::prelude::*;
use crate::enums::tls_error::TLSError;
use crate::structs::client_hello::ClientHello;
use crate::enums::protocol_version::ProtocolVersion;
use crate::utils::transmute;
use crate::enums::cipher_suite::CipherSuite;
use crate::enums::extension::Extension;
use crate::structs::key_share_entry::KeyShareEntry;
use crate::enums::named_group::NamedGroup;
use crate::enums::handshake::Handshake;
use crate::enums::alert_description::AlertDescription;
use crate::structs::server_hello::ServerHello;
use crate::criptographic_computation::*;
use crate::enums::protocol_name::ProtocolName;
use crate::enums::signature_scheme::SignatureScheme;
use crate::enums::psk_key_exchange_mode::PskKeyExchangeMode;
use crate::utils::transmute::EncodeTo;
use crate::structs::certificate_status_request::CertificateStatusRequest;
use crate::structs::certificate_status_type::CertificateStatusType;
use crate::structs::certificate_status_request::OCSPStatusRequest;
use crate::structs::tls_reader::HandshakeReader;
use crate::structs::tls_writer::HandshakeWriter;
use crate::structs::finished::Finished;
use crate::structs::tls_writer::TLSWriter;
use crate::structs::tls_reader::TLSReader;
use x509::asn1::decode::DecodeFrom;
use x509::rsa::rsa_primitives::rsassa_pkcs_verify;
use x509::public_key::PublicKey;
use x509::signature::Signature;
use x509::certificate::Certificate;
use x509::algorithm::AlgorithmParameters;
use x509::ecdsa::named_curve::NamedCurve;
use sha::{sha1, sha256, ecdh_x25519};
use ecc::domain_parameters::EccDomainParameters;
use x509::ecdsa::ecdsa_signature::EcdsaSignature;
use x509::oid;

pub struct ClientHandshake<R:Read, W:Write> {
    state: ClientSessionState,
    handshake_reader: Option<HandshakeReader<R>>,
    handshake_writer: Option<HandshakeWriter<W>>,
    messages: Box<[u8]>,
    private_key: [u8; 32],
    handshake_secret: [u8; 32],
    server_handshake_secret: [u8; 32],
    client_handshake_secret: [u8; 32],
    pub tls_reader: Option<TLSReader<R>>,
    pub tls_writer: Option<TLSWriter<W>>,
    server_certificate: Option<Certificate>,
}

impl<R:Read, W:Write> ClientHandshake<R, W> {
    pub fn new(stream_reader: R, stream_writer: W) -> ClientHandshake<R, W> {
        let session = ClientHandshake {
            state: ClientSessionState::Start,
            handshake_reader: Some(HandshakeReader::new(stream_reader)),
            handshake_writer: Some(HandshakeWriter::new(stream_writer)),
            messages: Box::from([]),
            private_key: random::x982::generate_bytes(),
            handshake_secret: [0; 32],
            server_handshake_secret: [0; 32],
            client_handshake_secret: [0; 32],
            tls_reader: None,
            tls_writer: None,
            server_certificate: None,
        };

        session
    }

    pub fn connect(&mut self) -> Result<(TLSReader<R>, TLSWriter<W>), TLSError> {
        self.send_client_hello()?;
        self.state = ClientSessionState::WaitServerHello;
        let (handshake_message, message_bytes) = self.handshake_reader.as_mut().unwrap().read()?;
        self.process_handshake(handshake_message, &message_bytes)?;
        self.handshake_reader.as_mut().unwrap().set_cipher_suit(CipherSuite::ChaCha20Poly1305Sha256);
        let (handshake_message, message_bytes) = self.handshake_reader.as_mut().unwrap().read()?;
        self.process_handshake(handshake_message, &message_bytes)?;
        let (handshake_message, message_bytes) = self.handshake_reader.as_mut().unwrap().read()?;
        self.process_handshake(handshake_message, &message_bytes)?;

        let (handshake_message, message_bytes) = self.handshake_reader.as_mut().unwrap().read()?;
        self.process_handshake( handshake_message, &message_bytes)?;

        let (handshake_message, message_bytes) = self.handshake_reader.as_mut().unwrap().read()?;
        self.process_handshake(handshake_message, &message_bytes)?;



        Ok((self.tls_reader.take().unwrap(), self.tls_writer.take().unwrap()))
    }

    fn send_handshake_bytes(&mut self, bytes: &[u8]) -> Result<(), TLSError> {
        self.handshake_writer.as_mut().unwrap().write(bytes)?;
        self.concat_message_bytes(bytes);
        Ok(())
    }

    fn send_client_hello(&mut self) -> Result<(), TLSError> {

        let mut u_coordinate: [u8; 32] = [0; 32];
        u_coordinate[0] = 9;

        let client_key_share: Box<[u8]> = Box::from(ecdh_x25519::x25519(self.private_key, u_coordinate));
        let client_key_share_entry = KeyShareEntry {
            group: NamedGroup::X25519,
            key_exchange: client_key_share,
        };

        let extensions: Box<[Extension]> = Box::from([
            Extension::SupportedGroups(Box::from([NamedGroup::X25519, NamedGroup::Secp256r1])),
            Extension::ApplicationLayerProtocolNegotiation(Box::from([ProtocolName::Http11])),
            Extension::SignatureAlgorithms {
                supported_signature_algorithms: Box::from([
                    SignatureScheme::EcdsaSecp256r1Sha256, SignatureScheme::RsaPssRsaeSha256, SignatureScheme::RsaPkcs1Sha256,
                    SignatureScheme::EcdsaSecp384r1Sha384, SignatureScheme::RsaPssRsaeSha384, SignatureScheme::RsaPkcs1Sha384,
                    SignatureScheme::RsaPssRsaeSha512, SignatureScheme::RsaPkcs1Sha512, SignatureScheme::RsaPkcs1Sha1
                ])
            },
            Extension::KeyShareClientHello { client_shares: Box::from([client_key_share_entry]) },
            Extension::PskKeyExchangeModes { ke_modes: Box::from([PskKeyExchangeMode::PskDheKe])},
            Extension::SupportedVersionsClientHello(Box::from([ProtocolVersion::TLS13])),
            Extension::CertificateStatusRequest(CertificateStatusRequest{
                status_type: CertificateStatusType::OCSP,
                request: OCSPStatusRequest {
                    responder_id_list: Box::from([]),
                    request_extensions: Box::from([]),
                }
            })
        ]);

        let client_hello = ClientHello {
            legacy_version: ProtocolVersion::TLS12,
            random: random::x982::generate_bytes(),
            legacy_session_id: Box::from([]),
            cipher_suites: Box::from([CipherSuite::ChaCha20Poly1305Sha256]),
            legacy_compression_methods: Box::from([0]),
            extensions
        };

        self.send_handshake_bytes(&Handshake::ClientHello(client_hello).encode())
    }

    fn process_handshake(&mut self, handshake: Handshake, message_bytes: &[u8]) -> Result<(), TLSError> {
        println!("====================================================");
        match handshake {
            Handshake::ServerHello(server_hello) => {
                dbg!("ServerHello");
                self.concat_message_bytes(message_bytes);
                self.state = ClientSessionState::WaitEncryptedExtensions;
                self.process_server_hello(&server_hello)?;
                Ok(())
            },
            Handshake::EncryptedExtensions(_) => {
                dbg!("EncryptedExtensions");
                self.concat_message_bytes(message_bytes);
                self.state = ClientSessionState::WaitCertificate;
                Ok(())
            },
            Handshake::Certificate(certificate) => {
                dbg!("Certificate");
                self.concat_message_bytes(message_bytes);
                if certificate.certificate_list.len() == 0 {
                    return Err(TLSError::Alert(AlertDescription::DecodeError));
                }
                let mut certificates:Vec<_> = Vec::with_capacity(certificate.certificate_list.len());
                certificates.push(Some(x509::certificate::Certificate::decode(&certificate.certificate_list[0].cert_data)?.0));
                for i in 1 .. certificate.certificate_list.len() {
                    let (certificate, _) = x509::certificate::Certificate::decode(&certificate.certificate_list[i].cert_data)?;
                    certificates.push(Some(certificate))
                }

                for i in 0 .. certificates.len()  {
                    let current_certificate = certificates[i].as_ref().unwrap();
                    let signer_certificate = if i + 1 < certificates.len() {
                        certificates[i + 1].as_ref().unwrap()
                    }
                    else {
                        if current_certificate.tbs_certificate.issuer != current_certificate.tbs_certificate.subject {
                            continue;
                        }
                        current_certificate
                    };

                    let result = Self::verify_signature(&signer_certificate.tbs_certificate.subject_public_key_info.public_key,
                            &current_certificate.signature, &current_certificate.tbs_certificate.der_encoded,
                            &current_certificate.tbs_certificate.subject_public_key_info.algorithm.parameters)?;

                    match result {
                        true => eprintln!("{}) CERTIFICATE CHECK: SUCCESSFUL", i),
                        false => {
                            eprintln!("{}) CERTIFICATE CHECK: FAILED", i);
                            return Err(TLSError::Alert(AlertDescription::BadCertificate))
                        },
                    };
                }
                self.server_certificate = Some(certificates[0].take().unwrap());
                self.state = ClientSessionState::WaitCertificateVerify;
                Ok(())
            },
            Handshake::CertificateVerify(certificate_verify) => {
                dbg!("CertificateVerify");
                let certificate = self.server_certificate.as_ref().unwrap();
                let subject_public_key_info = &certificate.tbs_certificate.subject_public_key_info;
                let (signature, parameters) = match certificate_verify.algorithm {
                    SignatureScheme::EcdsaSecp256r1Sha256 => {
                        let (signature, _) = EcdsaSignature::decode(&certificate_verify.signature)?;
                        (Signature::ECDSA(signature), AlgorithmParameters::Ecdsa(NamedCurve::Secp256r1))
                    },
                    SignatureScheme::RsaPkcs1Sha256 => {
                        (Signature::RSA(certificate_verify.signature.clone()), AlgorithmParameters::None)
                    },
                    _  => return Err(TLSError::Alert(AlertDescription::DecryptError)),
                };
                let content = self.get_server_signature_content();
                let result = Self::verify_signature(&subject_public_key_info.public_key, &signature, &content, &parameters)?;
                match result {
                    true => println!("PRIVATE KEY POSSESSION CHECK: SUCCESSFUL"),
                    false => {
                        println!("PRIVATE KEY POSSESSION CHECK: FAILED");
                        return Err(TLSError::Alert(AlertDescription::DecryptError))
                    },
                };
                self.concat_message_bytes(message_bytes);
                self.state = ClientSessionState::WaitFinished;
                Ok(())
            },
            Handshake::Finished(finished) => {
                dbg!("Finished");
                let expected = self.get_final_verify_data(&self.server_handshake_secret);
                if &expected != &finished.verify_data{
                    return Err(TLSError::Alert(AlertDescription::DecryptError));
                }

                self.concat_message_bytes(message_bytes);

                let master_secret = master_secret_sha256(&self.handshake_secret);
                let client_app_traffic_secret = client_application_traffic_secret_0_sha256(&master_secret, &self.messages);
                let server_app_traffic_secret = server_application_traffic_secret_0_sha256(&master_secret, &self.messages);

                self.send_handshake_bytes(&self.get_finished_message_bytes())?;

                let stream_writer = self.handshake_writer.take().unwrap().stream_writer;
                let stream_reader = self.handshake_reader.take().unwrap().stream_reader;

                self.tls_writer = Some(TLSWriter::new(stream_writer, CipherSuite::ChaCha20Poly1305Sha256,
                hkdf_expand_label_sha256(&client_app_traffic_secret, "key", &[], 32),
                hkdf_expand_label_sha256(&client_app_traffic_secret, "iv", &[], 12)
                ));

                self.tls_reader = Some(TLSReader::new(stream_reader,CipherSuite::ChaCha20Poly1305Sha256,
                    hkdf_expand_label_sha256(&server_app_traffic_secret, "key", &[], 32),
                    hkdf_expand_label_sha256(&server_app_traffic_secret, "iv", &[], 12),
                ));
                Ok(())
            },
            _ => Err(TLSError::Alert(AlertDescription::UnexpectedMessage))
        }
    }

    fn process_server_hello(&mut self, server_hello: &ServerHello) -> Result<(), TLSError> {
        server_hello.check_validity()?;
        let server_public_key = server_hello.get_server_share(NamedGroup::X25519)?;
        let (_, shorts, _) = unsafe { server_public_key.align_to::<[u8; 32]>() };
        let server_public_key = shorts[0];
        let shared_secret: Box<[u8]> = Box::from(ecdh_x25519::x25519(self.private_key, server_public_key));
        let early_secret = early_secret_sha256(&[0u8; 32]);
        let handshake_secret = handshake_secret_sha256(&shared_secret, &early_secret);
        self.handshake_secret = handshake_secret;
        let client_handshake_traffic_secret = client_handshake_traffic_secret_sha256(&handshake_secret, &self.messages);
        let server_handshake_traffic_secret = server_handshake_traffic_secret_sha256(&handshake_secret, &self.messages);
        self.server_handshake_secret = server_handshake_traffic_secret;
        self.client_handshake_secret = client_handshake_traffic_secret;
        let handshake_reader = self.handshake_reader.as_mut().unwrap();

        handshake_reader.set_sender_key(&hkdf_expand_label_sha256(&server_handshake_traffic_secret, "key", &[], 32),
         &hkdf_expand_label_sha256(&server_handshake_traffic_secret, "iv", &[], 12));
        handshake_reader.set_cipher_suit(CipherSuite::ChaCha20Poly1305Sha256);
        let handshake_writer = self.handshake_writer.as_mut().unwrap();
        handshake_writer.set_sender_key(&hkdf_expand_label_sha256(&client_handshake_traffic_secret, "key", &[], 32),
        &hkdf_expand_label_sha256(&client_handshake_traffic_secret, "iv", &[], 12));
        handshake_writer.set_cipher_suit(CipherSuite::ChaCha20Poly1305Sha256);

        Ok(())
    }

    fn get_final_verify_data(&self, handshake_traffic_secret:&[u8]) -> Box<[u8]> {
        let finished_key = hkdf_expand_label_sha256(&handshake_traffic_secret, "finished", &[], 32);
        let transcript_hash = transcript_hash_sha256(&self.messages);
        Box::from(sha::hmac::hmac_sha256(&finished_key, &transcript_hash))
    }

    fn get_finished_message_bytes(&self) -> Box<[u8]> {
        let finished = Finished {
            verify_data: self.get_final_verify_data(&self.client_handshake_secret),
        };
        Handshake::Finished(finished).encode()
    }

    fn verify_signature(public_key:&PublicKey, signature:&Signature, message:&[u8], parameters:&AlgorithmParameters) -> Result<bool, TLSError> {
        match public_key {
            PublicKey::RSA( ref public_key) => match signature {
                Signature::RSA(ref signature) => Ok(rsassa_pkcs_verify(public_key, message, signature, oid::SHA256)?),
                _ => Err(TLSError::Alert(AlertDescription::DecryptError)),
            },
            PublicKey::ECDSA(ref public_key) => {
                let (digest, domain_parameters) = match &parameters {
                    AlgorithmParameters::Ecdsa(curve) => {
                        match curve {
                            NamedCurve::Secp192r1 => (Box::from(&sha1::encode(message) as &[u8]) as Box<[u8]>, EccDomainParameters::new_p_192()),
                            NamedCurve::Secp256r1 => (Box::from(&sha256::encode(message) as &[u8]) as Box<[u8]>,EccDomainParameters::new_p_256()),
                            NamedCurve::Secp521r1 => (Box::from(&sha256::encode(message) as &[u8]) as Box<[u8]>,EccDomainParameters::new_p_521()),
                        }
                    },
                    _ => return Err(TLSError::Alert(AlertDescription::DecryptError)),
                };
                let digest = ecc::data_conversion::octets_to_integer(&digest);
                match signature {
                    Signature::ECDSA(signature) =>  {
                        let signature_r_part = ecc::data_conversion::octets_to_integer(&signature.r);
                        let signature_s_part = ecc::data_conversion::octets_to_integer(&signature.s);
                        let result = x509::ecdsa::ecdsa_primitives::ecdsa_verify_p(&signature_r_part, &signature_s_part,
                            public_key, &domain_parameters, &digest);
                        Ok(result)
                    },
                    _ => Err(TLSError::Alert(AlertDescription::DecryptError)),
                }
            },
            PublicKey::None => Err(TLSError::Alert(AlertDescription::DecryptError)),
        }
    }

    fn get_server_signature_content(&self) -> Box<[u8]> {
        let prefix: [u8; 64] = [0x20; 64];
        const CONTEXT_STRING: &str = "TLS 1.3, server CertificateVerify";
        let transcript_hash = transcript_hash_sha256(&self.messages);
        let mut content = Vec::with_capacity(64 + CONTEXT_STRING.len() + 1 + transcript_hash.len());
        content.extend_from_slice(&prefix);
        content.extend_from_slice(CONTEXT_STRING.as_bytes());
        content.push(0);
        content.extend_from_slice(&transcript_hash);
        content.into_boxed_slice()
    }

    #[inline]
    fn concat_message_bytes(&mut self, message_bytes:&[u8]){
//        dbg!("CONCAT MESSAGE");
        self.messages = transmute::concat_u8_slices(&self.messages, message_bytes);
    }
}
