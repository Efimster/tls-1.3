use crate::enums::session_state::ServerSessionState;
use std::io::prelude::*;
use crate::structs::client_hello::ClientHello;
use crate::enums::tls_error::TLSError;
use crate::enums::alert_description::AlertDescription;
use crate::utils::transmute;
use crate::structs::key_share_entry::KeyShareEntry;
use crate::enums::named_group::NamedGroup;
use crate::enums::extension::Extension;
use crate::structs::server_hello::ServerHello;
use crate::enums::protocol_version::ProtocolVersion;
use crate::enums::cipher_suite::CipherSuite;
use crate::structs::encrypted_extensions::EncryptedExtensions;
use crate::criptographic_computation::*;
use crate::enums::handshake::Handshake;
use crate::utils::transmute::EncodeTo;
use crate::structs::certificate::Certificate;
use crate::structs::certificate::CertificateEntry;
use crate::structs::certificate_verify::CertificateVerify;
use crate::enums::signature_scheme::SignatureScheme;
use x509::rsa::rsa_primitives;
use crate::structs::finished::Finished;
use crate::structs::tls_reader::HandshakeReader;
use crate::structs::tls_writer::HandshakeWriter;
use crate::structs::tls_reader::TLSReader;
use crate::structs::tls_writer::TLSWriter;
use x509::private_key::PrivateKey;
use sha::{sha256, ecdh_x25519};
use x509::ecdsa::ecdsa_primitives;
use x509::ecdsa::ecdsa_signature::EcdsaSignature;
use x509::asn1::encode::EncodeTo as Asn1EncodeTo;
use x509::oid;
use ecc::domain_parameters::EccDomainParameters;

pub struct ServerHandshake<'a, R:Read, W:Write> {
    state: ServerSessionState,
    handshake_reader: Option<HandshakeReader<R>>,
    handshake_writer: Option<HandshakeWriter<W>>,
    messages: Box<[u8]>,
    certificate_chain: &'a [&'a [u8]],
    certificate_private_key: &'a PrivateKey,
    handshake_secret: [u8; 32],
    client_handshake_secret: [u8; 32],
    signature_schema: SignatureScheme,
    pub tls_reader: Option<TLSReader<R>>,
    pub tls_writer: Option<TLSWriter<W>>,
}

impl<'a, R: Read, W: Write> ServerHandshake<'a, R, W> {
    pub fn new(stream_reader:R, stream_writer:W, certificates:&'a [&'a [u8]],
        certificate_private_key:&'a PrivateKey, signature_schema:SignatureScheme) -> ServerHandshake<'a, R, W> {

        let session = ServerHandshake {
            state: ServerSessionState::Start,
            handshake_reader: Some(HandshakeReader::new(stream_reader)),
            handshake_writer: Some(HandshakeWriter::new(stream_writer)),
            messages: Box::new([]),
            certificate_chain: certificates,
            certificate_private_key,
            signature_schema,
            handshake_secret: [0u8; 32],
            client_handshake_secret: [0; 32],
            tls_reader: None,
            tls_writer: None,
        };

        session
    }

    pub fn listen(&mut self) -> Result<(TLSReader<R>, TLSWriter<W>), TLSError>{
        let (handshake_message, message_bytes) = self.handshake_reader.as_mut().unwrap().read()?;
        self.process_handshake(handshake_message, &message_bytes)?;
        let (handshake_message, message_bytes) = self.handshake_reader.as_mut().unwrap().read()?;
        self.process_handshake(handshake_message, &message_bytes)?;

        Ok((self.tls_reader.take().unwrap(), self.tls_writer.take().unwrap()))
    }

    fn process_handshake(&mut self, handshake: Handshake, message_bytes: &[u8]) -> Result<(), TLSError> {
        println!("====================================================");
        match handshake {
            Handshake::ClientHello(client_hello) => {
                self.state = ServerSessionState::ReceivedClientHello;
                eprintln!("Client_hello {:?}", &client_hello);
                self.concat_message_bytes(&message_bytes);
                self.process_client_hello(&client_hello)?;
            },
            Handshake::Certificate(_) => {
                dbg!("Certificate");
                self.concat_message_bytes(&message_bytes);
            },
            Handshake::CertificateVerify(_) => {
                dbg!("CertificateVerify");
                //concat_message_bytes should be last thing here
                self.concat_message_bytes(&message_bytes);
            },
            Handshake::Finished(finished) => {
                dbg!("Finished");

                let expected = self.get_final_verify_data(&self.client_handshake_secret);
                if &expected != &finished.verify_data {
                    return Err(TLSError::Alert(AlertDescription::DecryptError));
                }

                let master_secret = master_secret_sha256(&self.handshake_secret);
                let client_app_traffic_secret = client_application_traffic_secret_0_sha256(&master_secret, &self.messages);
                let server_app_traffic_secret = server_application_traffic_secret_0_sha256(&master_secret, &self.messages);

                let stream_writer = self.handshake_writer.take().unwrap().stream_writer;
                let stream_reader = self.handshake_reader.take().unwrap().stream_reader;

                self.tls_writer = Some(TLSWriter::new(stream_writer, CipherSuite::ChaCha20Poly1305Sha256,
              hkdf_expand_label_sha256(&server_app_traffic_secret, "key", &[], 32),
              hkdf_expand_label_sha256(&server_app_traffic_secret, "iv", &[], 12)
                ));

                self.tls_reader = Some(TLSReader::new(stream_reader, CipherSuite::ChaCha20Poly1305Sha256,
              hkdf_expand_label_sha256(&client_app_traffic_secret, "key", &[], 32),
              hkdf_expand_label_sha256(&client_app_traffic_secret, "iv", &[], 12),
                ));
            },
            _ => {
                return Err(TLSError::Alert(AlertDescription::UnexpectedMessage));
            },
        }

        Ok(())
    }

    fn process_client_hello(&mut self, client_hello:&ClientHello) -> Result<(), TLSError>{
        client_hello.check_validity()?;
        match client_hello.get_client_share(NamedGroup::X25519) {
            Some(key_share) => {
                let private_key = random::x982::generate_bytes();
                self.send_server_hello(&client_hello.legacy_session_id, private_key)?;

                let (_, shorts, _) = unsafe { key_share.align_to::<[u8; 32]>() };
                let client_public_key = shorts[0];
                let shared_secret: Box<[u8]> = Box::new(ecdh_x25519::x25519(private_key, client_public_key));
                let early_secret = early_secret_sha256(&[0u8;32]);
                let handshake_secret = handshake_secret_sha256(&shared_secret, &early_secret);
                self.handshake_secret = handshake_secret;
                let server_handshake_traffic_secret = server_handshake_traffic_secret_sha256(&handshake_secret, &self.messages);
                let client_handshake_traffic_secret = client_handshake_traffic_secret_sha256(&handshake_secret, &self.messages);
                self.client_handshake_secret = client_handshake_traffic_secret;
                let handshake_writer = self.handshake_writer.as_mut().unwrap();
                handshake_writer.set_sender_key(&hkdf_expand_label_sha256(&server_handshake_traffic_secret, "key", &[], 32),
                &hkdf_expand_label_sha256(&server_handshake_traffic_secret, "iv", &[], 12));
                handshake_writer.set_cipher_suit(CipherSuite::ChaCha20Poly1305Sha256);
                let handshake_reader = self.handshake_reader.as_mut().unwrap();
                handshake_reader.set_sender_key(&hkdf_expand_label_sha256(&client_handshake_traffic_secret, "key", &[], 32),
        &hkdf_expand_label_sha256(&client_handshake_traffic_secret, "iv", &[], 12));
                handshake_reader.set_cipher_suit(CipherSuite::ChaCha20Poly1305Sha256);

                self.send_authentication_messages(&server_handshake_traffic_secret)?;

            },
            //TODO: send HelloRetryRequest instead
            None => return Err(TLSError::Alert(AlertDescription::InsufficientSecurity)),
        }

        self.state = ServerSessionState::Negotiated;

        Ok(())
    }

    fn send_handshake_bytes(&mut self, bytes: &[u8]) -> Result<(), TLSError> {
        self.handshake_writer.as_mut().unwrap().write(&bytes)?;
        Ok(())
    }

    fn send_server_hello(&mut self, legacy_session_id_echo:&[u8], private_key:[u8; 32]) -> Result<(), TLSError>
    {
        let mut u_coordinate:[u8;32] = [0; 32];
        u_coordinate[0] = 9;
        let server_public_key: Box<[u8]> = Box::new(ecdh_x25519::x25519(private_key, u_coordinate));

        let server_key_share_entry = KeyShareEntry {
            group: NamedGroup::X25519,
            key_exchange: server_public_key
        };

        let extensions:Box<[Extension]> = vec![
            Extension::SupportedVersionsServerHello { selected_version: ProtocolVersion::TLS13 },
            Extension::KeyShareServerHello(server_key_share_entry),
        ].into_boxed_slice();

        let server_hello = ServerHello {
            legacy_version: ProtocolVersion::TLS12,
            random: random::x982::generate_bytes(),
            legacy_session_id_echo: Box::from(legacy_session_id_echo),
            cipher_suit: CipherSuite::ChaCha20Poly1305Sha256,
            legacy_compression_method: 0u8,
            extensions
        };

        let bytes = &Handshake::ServerHello(server_hello).encode() as &[u8];
        self.concat_message_bytes(&bytes);
        self.send_handshake_bytes(&bytes)
    }

    fn send_authentication_messages(&mut self, server_handshake_traffic_secret:&[u8]) -> Result<(), TLSError> {
        let encrypted_extensions = Self::get_encrypted_extensions_message_bytes();
        self.concat_message_bytes(&encrypted_extensions);
        let certificate = self.get_certificate_message_bytes();
        self.concat_message_bytes(&certificate);
        let certificate_verify = self.get_server_certificate_verify_message_bytes()?;
        self.concat_message_bytes(&certificate_verify);
        let finished = self.get_finished_message_bytes(server_handshake_traffic_secret);
        self.concat_message_bytes(&finished);

        let mut bytes = Vec::with_capacity(encrypted_extensions.len() + certificate.len()
            + certificate_verify.len() + finished.len());
        bytes.extend_from_slice(&encrypted_extensions);
        bytes.extend_from_slice(&certificate);
        bytes.extend_from_slice(&certificate_verify);
        bytes.extend_from_slice(&finished);
        self.send_handshake_bytes(&bytes)
    }

    fn get_encrypted_extensions_message_bytes() -> Box<[u8]>{
        let extensions: Box<[Extension]> = vec![
            Extension::SupportedGroups(Box::new([NamedGroup::X25519])),
//            Extension::ApplicationLayerProtocolNegotiation(Box::new([ProtocolName::Http11])),
        ].into_boxed_slice();

        Handshake::EncryptedExtensions(EncryptedExtensions { extensions}).encode()
    }

    fn get_certificate_message_bytes(&self) -> Box<[u8]> {
        let mut entries = Vec::with_capacity(self.certificate_chain.len());

        for certificate in self.certificate_chain.iter().cloned() {
            entries.push(CertificateEntry {
                cert_data: Box::from(certificate),
                extensions: Box::new([]),
            });
        }

        let certificate = Certificate {
            certificate_request_context: Box::new([]),
            certificate_list: entries.into_boxed_slice(),
        };

        Handshake::Certificate(certificate).encode()
    }

    fn get_server_signature_content(&self) -> Box<[u8]> {
        let prefix: [u8; 64] = [0x20; 64];
        const CONTEXT_STRING: &str = "TLS 1.3, server CertificateVerify";
        let transcript_hash = transcript_hash_sha256(&self.messages);
        let mut content = Vec::with_capacity(64 + CONTEXT_STRING.len() + 1 + transcript_hash.len());
        content.extend_from_slice(&prefix);
        content.extend_from_slice(CONTEXT_STRING.as_bytes());
        content.push(0);
        content.extend_from_slice(&transcript_hash);
        content.into_boxed_slice()
    }

    fn get_server_certificate_verify_message_bytes(&self) -> Result<Box<[u8]>, TLSError> {
        let content = self.get_server_signature_content();
        let signature = match self.signature_schema {
            SignatureScheme::RsaPkcs1Sha256 => {
                let private_key = match self.certificate_private_key {
                    PrivateKey::RSA(key) => key,
                    _ => return Err(TLSError::Alert(AlertDescription::DecodeError)),
                };

                rsa_primitives::rsassa_pkcs_sign(private_key, &content, oid::SHA256)?
            },
            SignatureScheme::EcdsaSecp256r1Sha256 => {
                let private_key = match self.certificate_private_key {
                    PrivateKey::ECDSA(key) => key,
                    _ => return Err(TLSError::Alert(AlertDescription::DecodeError)),
                };
                let digest = ecc::data_conversion::octets_to_integer(&sha256::encode(&content));
                let ecc_domain_parameters = EccDomainParameters::new_p_256();
                let (ecc_secret_number, ecc_secret_number_inverse) = ecdsa_primitives::ecdsa_generate_secret_number_and_inverse(
                    &ecc_domain_parameters.point_prime_order, 256, None);
                let (r, s) = ecdsa_primitives::ecdsa_sign_p(private_key, &ecc_domain_parameters,
                    &digest, &ecc_secret_number, &ecc_secret_number_inverse);
                let signature = EcdsaSignature { r: ecc::data_conversion::integer_to_octets(&r), s: ecc::data_conversion::integer_to_octets(&s) };
                signature.encode()
            },
            _ => return Err(TLSError::Alert(AlertDescription::InsufficientSecurity)),
        };
        let certificate_verify = CertificateVerify {
            algorithm: self.signature_schema,
            signature
        };
        Ok(Handshake::CertificateVerify(certificate_verify).encode())
    }

    fn get_final_verify_data(&self, handshake_traffic_secret: &[u8]) -> Box<[u8]> {
        let finished_key = hkdf_expand_label_sha256(&handshake_traffic_secret, "finished", &[], 32);
        let transcript_hash = transcript_hash_sha256(&self.messages);
        Box::new(sha::hmac::hmac_sha256(&finished_key, &transcript_hash))
    }

    fn get_finished_message_bytes(&self, server_handshake_traffic_secret:&[u8]) -> Box<[u8]> {
        let finished = Finished {
            verify_data: self.get_final_verify_data(server_handshake_traffic_secret),
        };
        Handshake::Finished(finished).encode()
    }

    #[inline]
    fn concat_message_bytes(&mut self, message_bytes: &[u8]) {
//        dbg!("CONCAT MESSAGE");
        self.messages = transmute::concat_u8_slices(&self.messages, message_bytes);
    }

}


#[cfg(test)]
mod tests {
    use super::*;
    use sha::sha256;

    #[test]
    fn test_key_generation(){
        let mut u_coordinate: [u8; 32] = [0; 32];
        u_coordinate[0] = 9;

        let private_key: [u8; 32] = [0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b, 0x9c, 0x9d, 0x9e, 0x9f, 0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf];

        let server_public_key = ecdh_x25519::x25519(private_key, u_coordinate);
        assert_eq!(&server_public_key, &[0x9f, 0xd7, 0xad, 0x6d, 0xcf, 0xf4, 0x29, 0x8d, 0xd3, 0xf9, 0x6d, 0x5b, 0x1b, 0x2a, 0xf9, 0x10, 0xa0, 0x53, 0x5b, 0x14, 0x88, 0xd7, 0xf8, 0xfa, 0xbb, 0x34, 0x9a, 0x98, 0x28, 0x80, 0xb6, 0x15]);
        let client_public_key:[u8; 32] = [0x35, 0x80, 0x72, 0xd6, 0x36, 0x58, 0x80, 0xd1, 0xae, 0xea, 0x32, 0x9a, 0xdf, 0x91, 0x21, 0x38, 0x38, 0x51, 0xed, 0x21, 0xa2, 0x8e, 0x3b, 0x75, 0xe9, 0x65, 0xd0, 0xd2, 0xcd, 0x16, 0x62, 0x54];
        let shared_key = ecdh_x25519::x25519(private_key, client_public_key);
        assert_eq!(&shared_key, &[0xdf, 0x4a, 0x29, 0x1b, 0xaa, 0x1e, 0xb7, 0xcf, 0xa6, 0x93, 0x4b, 0x29, 0xb4, 0x74, 0xba, 0xad, 0x26, 0x97, 0xe2, 0x9f, 0x1f, 0x92, 0x0d, 0xcc, 0x77, 0xc8, 0xa0, 0xa0, 0x88, 0x44, 0x76, 0x24]);
        let client_hello_handshake_bytes:[u8; 202] = [0x01, 0x00, 0x00, 0xc6, 0x03, 0x03, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f, 0x20, 0xe0, 0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6, 0xe7, 0xe8, 0xe9, 0xea, 0xeb, 0xec, 0xed, 0xee, 0xef, 0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8, 0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff, 0x00, 0x06, 0x13, 0x01, 0x13, 0x02, 0x13, 0x03, 0x01, 0x00, 0x00, 0x77, 0x00, 0x00, 0x00, 0x18, 0x00, 0x16, 0x00, 0x00, 0x13, 0x65, 0x78, 0x61, 0x6d, 0x70, 0x6c, 0x65, 0x2e, 0x75, 0x6c, 0x66, 0x68, 0x65, 0x69, 0x6d, 0x2e, 0x6e, 0x65, 0x74, 0x00, 0x0a, 0x00, 0x08, 0x00, 0x06, 0x00, 0x1d, 0x00, 0x17, 0x00, 0x18, 0x00, 0x0d, 0x00, 0x14, 0x00, 0x12, 0x04, 0x03, 0x08, 0x04, 0x04, 0x01, 0x05, 0x03, 0x08, 0x05, 0x05, 0x01, 0x08, 0x06, 0x06, 0x01, 0x02, 0x01, 0x00, 0x33, 0x00, 0x26, 0x00, 0x24, 0x00, 0x1d, 0x00, 0x20, 0x35, 0x80, 0x72, 0xd6, 0x36, 0x58, 0x80, 0xd1, 0xae, 0xea, 0x32, 0x9a, 0xdf, 0x91, 0x21, 0x38, 0x38, 0x51, 0xed, 0x21, 0xa2, 0x8e, 0x3b, 0x75, 0xe9, 0x65, 0xd0, 0xd2, 0xcd, 0x16, 0x62, 0x54, 0x00, 0x2d, 0x00, 0x02, 0x01, 0x01, 0x00, 0x2b, 0x00, 0x03, 0x02, 0x03, 0x04];
        let server_hello_handshake_bytes:[u8; 122] = [0x02, 0x00, 0x00, 0x76, 0x03, 0x03, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7a, 0x7b, 0x7c, 0x7d, 0x7e, 0x7f, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f, 0x20, 0xe0, 0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6, 0xe7, 0xe8, 0xe9, 0xea, 0xeb, 0xec, 0xed, 0xee, 0xef, 0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8, 0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff, 0x13, 0x01, 0x00, 0x00, 0x2e, 0x00, 0x33, 0x00, 0x24, 0x00, 0x1d, 0x00, 0x20, 0x9f, 0xd7, 0xad, 0x6d, 0xcf, 0xf4, 0x29, 0x8d, 0xd3, 0xf9, 0x6d, 0x5b, 0x1b, 0x2a, 0xf9, 0x10, 0xa0, 0x53, 0x5b, 0x14, 0x88, 0xd7, 0xf8, 0xfa, 0xbb, 0x34, 0x9a, 0x98, 0x28, 0x80, 0xb6, 0x15, 0x00, 0x2b, 0x00, 0x02, 0x03, 0x04];
        let hello_messages = transmute::concat_u8_slices(&client_hello_handshake_bytes, &server_hello_handshake_bytes);
        assert_eq!(&sha256::encode(&hello_messages), &[0xda, 0x75, 0xce, 0x11, 0x39, 0xac, 0x80, 0xda, 0xe4, 0x04, 0x4d, 0xa9, 0x32, 0x35, 0x0c, 0xf6, 0x5c, 0x97, 0xcc, 0xc9, 0xe3, 0x3f, 0x1e, 0x6f, 0x7d, 0x2d, 0x4b, 0x18, 0xb7, 0x36, 0xff, 0xd5]);
        let hello_hash = transcript_hash_sha256(&hello_messages);
        assert_eq!(&hello_hash, &[0xda, 0x75, 0xce, 0x11, 0x39, 0xac, 0x80, 0xda, 0xe4, 0x04, 0x4d, 0xa9, 0x32, 0x35, 0x0c, 0xf6, 0x5c, 0x97, 0xcc, 0xc9, 0xe3, 0x3f, 0x1e, 0x6f, 0x7d, 0x2d, 0x4b, 0x18, 0xb7, 0x36, 0xff, 0xd5]);

        let early_secret = early_secret_sha256(&[0u8;32]);
        assert_eq!(&early_secret, &[0x33, 0xad, 0x0a, 0x1c, 0x60, 0x7e, 0xc0, 0x3b, 0x09, 0xe6, 0xcd, 0x98, 0x93, 0x68, 0x0c, 0xe2, 0x10, 0xad, 0xf3, 0x00, 0xaa, 0x1f, 0x26, 0x60, 0xe1, 0xb2, 0x2e, 0x10, 0xf1, 0x70, 0xf9, 0x2a]);
        let empty_hash = sha256::encode(&[]);
        assert_eq!(&empty_hash, &[0xe3, 0xb0, 0xc4, 0x42, 0x98, 0xfc, 0x1c, 0x14, 0x9a, 0xfb, 0xf4, 0xc8, 0x99, 0x6f, 0xb9, 0x24, 0x27, 0xae, 0x41, 0xe4, 0x64, 0x9b, 0x93, 0x4c, 0xa4, 0x95, 0x99, 0x1b, 0x78, 0x52, 0xb8, 0x55]);
        let derive_secret = derive_secrete_sha256(&early_secret, "derived", &[]);
        assert_eq!(&derive_secret, &[0x6f, 0x26, 0x15, 0xa1, 0x08, 0xc7, 0x02, 0xc5, 0x67, 0x8f, 0x54, 0xfc, 0x9d, 0xba, 0xb6, 0x97, 0x16, 0xc0, 0x76, 0x18, 0x9c, 0x48, 0x25, 0x0c, 0xeb, 0xea, 0xc3, 0x57, 0x6c, 0x36, 0x11, 0xba]);
        let handshake_secret = handshake_secret_sha256(&shared_key, &early_secret);
        assert_eq!(&handshake_secret, &[0xfb, 0x9f, 0xc8, 0x06, 0x89, 0xb3, 0xa5, 0xd0, 0x2c, 0x33, 0x24, 0x3b, 0xf6, 0x9a, 0x1b, 0x1b, 0x20, 0x70, 0x55, 0x88, 0xa7, 0x94, 0x30, 0x4a, 0x6e, 0x71, 0x20, 0x15, 0x5e, 0xdf, 0x14, 0x9a]);

        let server_handshake_traffic_secret = server_handshake_traffic_secret_sha256(&handshake_secret, &hello_messages);
        assert_eq!(&server_handshake_traffic_secret, &[0xa2, 0x06, 0x72, 0x65, 0xe7, 0xf0, 0x65, 0x2a, 0x92, 0x3d, 0x5d, 0x72, 0xab, 0x04, 0x67, 0xc4, 0x61, 0x32, 0xee, 0xb9, 0x68, 0xb6, 0xa3, 0x2d, 0x31, 0x1c, 0x80, 0x58, 0x68, 0x54, 0x88, 0x14]);
        let client_handshake_traffic_secret = client_handshake_traffic_secret_sha256(&handshake_secret, &hello_messages);
        assert_eq!(&client_handshake_traffic_secret, &[0xff, 0x0e, 0x5b, 0x96, 0x52, 0x91, 0xc6, 0x08, 0xc1, 0xe8, 0xcd, 0x26, 0x7e, 0xef, 0xc0, 0xaf, 0xcc, 0x5e, 0x98, 0xa2, 0x78, 0x63, 0x73, 0xf0, 0xdb, 0x47, 0xb0, 0x47, 0x86, 0xd7, 0x2a, 0xea]);

        let server_handshake_key:&[u8] = &hkdf_expand_label_sha256(&server_handshake_traffic_secret, "key", &[], 16);
        assert_eq!(server_handshake_key, &[0x84, 0x47, 0x80, 0xa7, 0xac, 0xad, 0x9f, 0x98, 0x0f, 0xa2, 0x5c, 0x11, 0x4e, 0x43, 0x40, 0x2a]);
        let client_handshake_key: &[u8] = &hkdf_expand_label_sha256(&client_handshake_traffic_secret, "key", &[], 16);
        assert_eq!(client_handshake_key, &[0x71, 0x54, 0xf3, 0x14, 0xe6, 0xbe, 0x7d, 0xc0, 0x08, 0xdf, 0x2c, 0x83, 0x2b, 0xaa, 0x1d, 0x39]);
        let server_handshake_iv: &[u8] = &hkdf_expand_label_sha256(&server_handshake_traffic_secret, "iv", &[], 12);
        assert_eq!(server_handshake_iv, &[0x4c, 0x04, 0x2d, 0xdc, 0x12, 0x0a, 0x38, 0xd1, 0x41, 0x7f, 0xc8, 0x15]);
        let client_handshake_iv: &[u8] = &hkdf_expand_label_sha256(&client_handshake_traffic_secret, "iv", &[], 12);
        assert_eq!(client_handshake_iv, &[0x71, 0xab, 0xc2, 0xca, 0xe4, 0xc6, 0x99, 0xd4, 0x7c, 0x60, 0x02, 0x68]);
    }
}