use crate::utils::transmute::EncodeTo;
use crate::utils::transmute::DecodeFrom;
use crate::enums::tls_error::TLSError;
use crate::utils::transmute;
use crate::structs::certificate_status_type::CertificateStatusType;

#[derive(Debug)]
pub struct CertificateStatusRequest {
    pub status_type: CertificateStatusType,
    pub request: OCSPStatusRequest,
}

impl EncodeTo for CertificateStatusRequest {
    fn encode_to(&self, to: &mut Vec<u8>) {
        self.status_type.encode_to(to);
        self.request.encode_to(to);
    }
}

impl<'a> DecodeFrom<'a> for CertificateStatusRequest {
    fn decode(bytes: &'a [u8]) -> Result<(Self, usize, &'a [u8]), TLSError> {
        let (status_type, status_type_size, bytes) = CertificateStatusType::decode(bytes)?;
        let (request, request_size, bytes) = OCSPStatusRequest::decode(bytes)?;

        Ok((CertificateStatusRequest {
            status_type,
            request,
        }, status_type_size + request_size, bytes))
    }
}

#[derive(Debug)]
pub struct OCSPStatusRequest {
    pub responder_id_list: Box<[ResponderID]>, //< 0..2 ^ 16 - 1 >;
    pub request_extensions: Box<[u8]>,
}

pub type ResponderID = Box<[u8]>;

impl EncodeTo for OCSPStatusRequest {
    fn encode_to(&self, to: &mut Vec<u8>) {
        let mut responder_id_list = Vec::new();
        for i in 0 .. self.responder_id_list.len() {
            let responder:&[u8] = &self.responder_id_list[i];
            transmute::encode_slice_to(&mut responder_id_list, responder, 2);
        }
        transmute::encode_slice_to(to, &responder_id_list, 2);
        transmute::encode_slice_to(to, &self.request_extensions, 2);
    }
}

impl<'a> DecodeFrom<'a> for OCSPStatusRequest {
    fn decode(bytes: &'a [u8]) -> Result<(Self, usize, &'a [u8]), TLSError> {
        let (mut data, responder_id_list_size, bytes) = transmute::decode_slice(bytes, 2);
        let mut responder_id_list = Vec::new();
        while data.len() > 0 {
            let (responder, size, _) = transmute::decode_slice(data,2);
            responder_id_list.push(Box::from(responder));
            data = &data[size..];
        }
        let (request_extensions, request_extensions_size, bytes) = transmute::decode_slice(bytes, 2);

        Ok((OCSPStatusRequest{
            responder_id_list: responder_id_list.into_boxed_slice(),
            request_extensions: Box::from(request_extensions),
        }, responder_id_list_size + request_extensions_size, bytes))
    }
}