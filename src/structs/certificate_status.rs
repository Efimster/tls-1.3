use crate::structs::certificate_status_type::CertificateStatusType;
use crate::utils::transmute::EncodeTo;
use crate::utils::transmute::DecodeFrom;
use crate::enums::tls_error::TLSError;
use crate::utils::transmute;

#[derive(Debug)]
pub struct CertificateStatus {
    pub status_type: CertificateStatusType,
    pub response: Box<[u8]>, //<1..2^24-1>
}

impl EncodeTo for CertificateStatus {
    fn encode_to(&self, to: &mut Vec<u8>) {
        self.status_type.encode_to(to);
        transmute::encode_slice_to(to, &self.response, 3);
    }
}

impl<'a> DecodeFrom<'a> for CertificateStatus {
    fn decode(bytes: &'a [u8]) -> Result<(Self, usize, &'a [u8]), TLSError> {
        let (status_type, status_type_size, bytes) = CertificateStatusType::decode(bytes)?;
        let (response, response_size, bytes) = transmute::decode_slice(bytes, 3);

        Ok((CertificateStatus {
            status_type,
            response: Box::from(response),
        }, status_type_size + response_size, bytes))
    }
}