use std::io::prelude::*;
use crate::enums::tls_record_content_type::*;
use crate::enums::tls_record_content_type;
use crate::enums::tls_error::TLSError;
use crate::utils::transmute;
use crate::enums::protocol_version::ProtocolVersion;
use crate::enums::cipher_suite::CipherSuite;
use crate::enums::alert_description::AlertDescription;
use crate::utils::transmute::EncodeTo;
use crate::utils::transmute::DecodeFrom;
use crate::structs::tls_inner_plain_text::TLSInnerPlainText;
use crate::structs::tls_plain_text::TLSPlainText;
use sha::cha_cha_poly::cha_cha_20_aead_decrypt;


#[derive(Debug)]
pub struct TLSCipherText {
    pub encrypted_record: Box<[u8]>,
}

impl TLSCipherText {

    pub fn read<R: Read>(stream_reader: &mut R, cipher_suite: CipherSuite, read_sequence_number: u64,
            sender_write_key: &[u8], sender_write_iv: &[u8]) -> Result<TLSPlainText, TLSError> {
        let record = TLSPlainText::read(stream_reader)?.to_cipher_text();
        let encode_len_bytes = transmute::u16_to_bytes_be(record.encrypted_record.len() as u16);
        let additional_data: [u8; 5] = [tls_record_content_type::APPLICATION_DATA, 0x03, 0x03,
            encode_len_bytes[0], encode_len_bytes[1]];

        match cipher_suite {
            CipherSuite::ChaCha20Poly1305Sha256 => {
                let mut write_key: [u8; 32] = [0; 32];
                write_key.copy_from_slice(&sender_write_key);

                let mut nonce: [u8; 12] = [0; 12];
                &nonce[4..].copy_from_slice(&transmute::u64_to_bytes_be(read_sequence_number));
                for i in 0..nonce.len() {
                    nonce[i] ^= sender_write_iv[i];
                }
                let encrypted_tag = &record.encrypted_record[record.encrypted_record.len() - 16 .. ];
                let cipher_text = &record.encrypted_record[.. record.encrypted_record.len() - 16];

                let (plain, tag) = cha_cha_20_aead_decrypt(&additional_data,
                    &write_key, nonce, cipher_text);


                for i in 0 .. 16 {
                    if tag[i] != encrypted_tag[i] {
                        return Err(TLSError::Alert(AlertDescription::BadRecordMac));
                    }
                }

                let mut decrypted_record: Vec<u8> = Vec::with_capacity(plain.len() + tag.len());
                decrypted_record.extend_from_slice(&plain);
                let mut index:usize = decrypted_record.len() - 1;
                while decrypted_record[index] == 0 {
                    index -= 1;
                }

                let (content_type, _, _) = TLSRecordContentType::decode(&decrypted_record[index..])?;
                let tls_inner_text = TLSInnerPlainText {
                    content: Box::from(&decrypted_record[..index]),
                    content_type,
                    zeros: Box::from(&decrypted_record[index + 1..]),
                };
                Ok(tls_inner_text.to_plain_text())
            },
            _ => Err(TLSError::Alert(AlertDescription::DecryptError)),
        }
    }

    pub fn encode_to(&self, to: &mut Vec<u8>) {
        TLSRecordContentType::ApplicationData.encode_to(to);
        ProtocolVersion::TLS12.encode_to(to);
        transmute::encode_u16_to(to, self.encrypted_record.len() as u16);
        to.extend_from_slice(&self.encrypted_record);
    }

    pub fn write<W: Write>(stream_writer: &mut W, data: &[u8], content_type: TLSRecordContentType,
        cipher_suite: CipherSuite, sequence_number: u64, sender_write_key: &[u8], sender_write_iv: &[u8]) -> Result<(), TLSError> {

        let mut fragment: Vec<u8> = Vec::with_capacity(data.len());
        fragment.extend_from_slice(data);

        let inner_text = TLSInnerPlainText {
            content: fragment.into_boxed_slice(),
            content_type,
            zeros: Box::new([]),
        };

        let tls_cipher_text = inner_text.to_tls_cipher_text(cipher_suite, sequence_number,
            sender_write_key, sender_write_iv)?;
        let mut bytes: Vec<u8> = Vec::with_capacity(tls_cipher_text.encode_len());
        tls_cipher_text.encode_to(&mut bytes);
        stream_writer.write(&bytes)?;
        Ok(())
    }

    #[inline]
    fn encode_len(&self) -> usize {
        self.encrypted_record.len() + 5
    }
}

