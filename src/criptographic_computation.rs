use sha::hkdf;
use sha::sha256;
use crate::utils::transmute;

//const TLS13STR:[u8; 6] = [0x74, 0x6c, 0x73, 0x31, 0x33, 0x20];
//const EXT_BINDER:[u8; 10] = [0x65, 0x78, 0x74, 0x20, 0x62, 0x69, 0x6e, 0x64, 0x65, 0x72];
//const RES_BINDER:[u8; 10] = [0x72, 0x65, 0x73, 0x20, 0x62, 0x69, 0x6e, 0x64, 0x65, 0x72];
//const C_E_TRAFFIC:[u8; 11] = [0x63, 0x20, 0x65, 0x20, 0x74, 0x72, 0x61, 0x66, 0x66, 0x69, 0x63];
//const E_EXP_MASTER:[u8; 12] = [0x65, 0x20, 0x65, 0x78, 0x70, 0x20, 0x6d, 0x61, 0x73, 0x74, 0x65, 0x72];
//const DERIVED:[u8; 7] = [0x64, 0x65, 0x72, 0x69, 0x76, 0x65, 0x64];
//const C_HS_TRAFFIC:[u8; 12] = [0x63, 0x20, 0x68, 0x73, 0x20, 0x74, 0x72, 0x61, 0x66, 0x66, 0x69, 0x63];
//const S_HS_TRAFFIC:[u8; 12] = [0x73, 0x20, 0x68, 0x73, 0x20, 0x74, 0x72, 0x61, 0x66, 0x66, 0x69, 0x63];
//const C_AP_TRAFFIC: [u8; 12] = [0x63, 0x20, 0x61, 0x70, 0x20, 0x74, 0x72, 0x61, 0x66, 0x66, 0x69, 0x63];
//const S_AP_TRAFFIC: [u8; 12] = [0x73, 0x20, 0x61, 0x70, 0x20, 0x74, 0x72, 0x61, 0x66, 0x66, 0x69, 0x63];
//const EXP_MASTER: [u8; 10] = [0x65, 0x78, 0x70, 0x20, 0x6d, 0x61, 0x73, 0x74, 0x65, 0x72];
//const RES_MASTER: [u8; 10] = [0x72, 0x65, 0x73, 0x20, 0x6d, 0x61, 0x73, 0x74, 0x65, 0x72];

//most probably output is 32 bytes long
pub fn derive_secrete_sha256(secret:&[u8], label:&str, messages:&[u8]) -> [u8; 32] {
    let mut result:[u8;32] = [0; 32];
    let expand = hkdf_expand_label_sha256(secret, label, &transcript_hash_sha256(messages), 32);
    result.copy_from_slice(&expand);
    result
}

pub fn hkdf_expand_label_sha256(secret:&[u8], label:&str, context:&[u8], length: usize) -> Box<[u8]>{
    let label = hkdf_label(length as u16, label, context);
    hkdf::expand_sha256(secret, &label, length)
}

pub fn hkdf_label(length:u16, label:&str, context:&[u8]) -> Box<[u8]> {
    let mut to:Vec<u8> = Vec::with_capacity(2 + label.len() + context.len());
    transmute::encode_u16_to(&mut to, length);
    transmute::encode_slice_to(&mut to, format!("tls13 {}", label).as_bytes(), 1);
    transmute::encode_slice_to(&mut to, context, 1);
    to.into_boxed_slice()
}

pub fn transcript_hash_sha256(concatenated_handshake_messages:&[u8]) -> [u8; 32]{
    sha256::encode(concatenated_handshake_messages)
}

pub fn early_secret_sha256(psk:&[u8]) -> [u8; 32]{
    let salt = [0u8; 32];
    hkdf::extract_sha256(&salt, psk)
}

//pub fn binder_key_sha256(early_secret:&[u8], resumption:bool) -> [u8; 32]{
//    let label = match resumption {
//        true => "res binder",
//        false => "ext binder"
//    };
//
//    derive_secrete_sha256(early_secret, &label, &[])
//}
//
//pub fn client_early_traffic_secret_sha256(early_secret: &[u8], client_hello_message:&[u8]) -> [u8; 32]{
//    derive_secrete_sha256(early_secret, "c e traffic", client_hello_message)
//}
//
//pub fn early_exporter_master_secret_sha256(early_secret: &[u8], client_hello_message: &[u8]) -> [u8; 32] {
//    derive_secrete_sha256(early_secret, "e exp master", client_hello_message)
//}

pub fn handshake_secret_sha256(shared_secret:&[u8], early_secret:&[u8]) -> [u8; 32] {
    let salt = derive_secrete_sha256(early_secret, "derived", &[]);
    hkdf::extract_sha256(&salt, shared_secret)
}

pub fn client_handshake_traffic_secret_sha256(handshake_secret: &[u8], client_hello_to_server_hello_messages: &[u8]) -> [u8; 32] {
    derive_secrete_sha256(handshake_secret, "c hs traffic", client_hello_to_server_hello_messages)
}

pub fn server_handshake_traffic_secret_sha256(handshake_secret: &[u8], client_hello_to_server_hello_messages: &[u8]) -> [u8; 32] {
    derive_secrete_sha256(handshake_secret, "s hs traffic", client_hello_to_server_hello_messages)
}

pub fn master_secret_sha256(handshake_secret: &[u8]) -> [u8; 32] {
    let salt = derive_secrete_sha256(handshake_secret, "derived", &[]);
    let ikm= [0u8; 32];
    hkdf::extract_sha256(&salt, &ikm)
}

pub fn client_application_traffic_secret_0_sha256(master_secret: &[u8], client_hello_to_server_finished_messages: &[u8]) -> [u8; 32] {
    derive_secrete_sha256(master_secret, "c ap traffic", client_hello_to_server_finished_messages)
}

pub fn server_application_traffic_secret_0_sha256(master_secret: &[u8], client_hello_to_server_finished_messages: &[u8]) -> [u8; 32] {
    derive_secrete_sha256(master_secret, "s ap traffic", client_hello_to_server_finished_messages)
}

//pub fn exporter_master_secret_sha256(master_secret: &[u8], client_hello_to_server_finished_messages: &[u8]) -> [u8; 32] {
//    derive_secrete_sha256(master_secret, "exp master", client_hello_to_server_finished_messages)
//}
//
//pub fn resumption_master_secret_sha256(master_secret: &[u8], client_hello_to_server_finished_messages: &[u8]) -> [u8; 32] {
//    derive_secrete_sha256(master_secret, "res master", client_hello_to_server_finished_messages)
//}